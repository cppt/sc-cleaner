FROM alpine:3.8

ENV LANG C.UTF-8
ENV WORK_DIR /opt/sc-cleaner

COPY . $WORK_DIR

WORKDIR $WORK_DIR

RUN apk --no-cache add build-base python3 python3-dev postgresql \
                       postgresql-dev \
    && python3 -m ensurepip \
    && pip3 install --upgrade pip \
    && pip3 install --no-cache-dir --upgrade -r requirements.txt \
    && pip3 install . \
    && pip3 uninstall -y pip \
    && apk del build-base python3-dev postgresql-dev

USER guest

ENTRYPOINT ["sc-cleaner"]
