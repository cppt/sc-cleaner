#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='sc-cleaner',
    version='1.2',
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'sc-cleaner = sc_cleaner.sc_cleaner:main',
        ],
    },
)
