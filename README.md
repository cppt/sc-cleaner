README
======

The `sc-cleaner` script cleans up a seiscomp3 database.

It removes unused `Pick` and `Amplitude`, but also unused `Originreference` and
lost `Object` (`Object` only existing in the `object` and `publicobject`
tables).

It also removes and clean  all `QualityControl` associated objects created by the seiscomp3 service `scqc`.
This option disable by default will clea-up older objects from tables Waveformquality, Outage and Qclog.
See GemPa documentation [here](https://docs.gempa.de/seiscomp3/current/base/api-python.html). 

It supports version 0.11 of seiscomp PostgreSQL database.

> **Careful**
>
> This script was designed to work with PostgreSQL and a specific version of
> the SeisComP3 database. It may work with MySQL and other SeisComP3 database
> version but you should know what you do.

Usage
-----

The database to clean up is defined by the `DATABASE` environment variable. It
is also possible to pass the database as an argument:

```bash
$> sc-cleaner -d postgresql://user:password@hostname/database
```

The objects of the last 30 days aren't cleaned up by default. This can be
changed by using the `--day` argument.

The `--limit` specifies the number of objects to remove in a single
transaction. By default, this value is set to 10000.

The `-t`, or `--target`, allow to only clean the desired objects:

```bash
$> sc-cleaner -t {amplitude,pick,originreference,object}
```

The `-st`,`--starttime`, allow to clean for a specific time range defined with `--day` or `-ed`,`--endtime`, see examples below :

```bash 
$> sc-cleaner -v -d postgresql://user:password@hostname/database -t {amplitude,pick,originreference} --limit 50000 -st 2019-07-30T00:00:00 --day 31
```
```
2019-10-03 20:09:53 [INFO] Objects targeted : ['amplitude', 'pick', 'originreference']
2019-10-03 20:09:53 [INFO] Max number of deleted object in a transaction : 50000 elements
2019-10-03 20:09:53 [INFO] Remove targeted unassociated elements found between
				[2019-07-30 00:00:00] - [2019-08-30 00:00:00]
2019-10-03 20:09:53 [INFO] Database schema-version : 0.11
2019-10-03 20:09:53 [INFO] Clean up Amplitude
2019-10-03 20:12:56 [INFO] ...  query 1 : 50000 obj deleted in 182.66 s
(...)
```
> In this case the number of days given by '--day' are added to the '--starttime' option 

Another way

```bash 
$> /usr/bin/sc-cleaner -v -d postgresql://user:password@hostname/database -t pick --limit 10000 -ed 2019-07-30T00:00:00 --day 1
```
```
2019-10-03 21:31:43 [INFO] Objects targeted : ['pick']
2019-10-03 21:31:43 [INFO] Max number of deleted object in a transaction : 10000 elements
2019-10-03 21:31:43 [INFO] Remove targeted unassociated elements found between
				[2019-07-29 00:00:00] - [2019-07-30 00:00:00]
2019-10-03 21:31:43 [INFO] Database schema-version : 0.11
2019-10-03 21:31:43 [INFO] Clean up Pick
2019-10-03 21:31:43 [INFO] ...  query 1 : 0 obj deleted in 0.19 s
2019-10-03 21:31:43 [INFO] 1 queries executed
2019-10-03 21:31:43 [INFO] 0 Picks deleted in 0.19 seconds
2019-10-03 21:31:43 [INFO] Total execution time: 0.2 seconds
```
> Here we define the endtime limit using '-ed' or '--endtime', the time range is then defined 
> using '--day' that is substracted to end time to obtain the start timestamp.


Another way

```bash 
$> sc-cleaner -v -d postgresql://user:password@hostname/database -t pick --limit 10000 -st 2019-09-15T00:00:00 -ed 2019-09-16T14:30:00
```
```
2019-10-03 21:36:13 [INFO] Objects targeted : ['pick']
2019-10-03 21:36:13 [INFO] Max number of deleted object in a transaction : 10000 elements
2019-10-03 21:36:13 [INFO] Remove targeted unassociated elements found between
				[2019-09-15 00:00:00] - [2019-09-16 14:30:00]
2019-10-03 21:36:13 [INFO] Database schema-version : 0.11
2019-10-03 21:36:13 [INFO] Clean up Pick
2019-10-03 21:36:16 [INFO] ...  query 1 : 3367 obj deleted in 3.31 s
2019-10-03 21:36:16 [INFO] 1 queries executed
2019-10-03 21:36:16 [INFO] 3367 Picks deleted in 3.31 seconds
2019-10-03 21:36:16 [INFO] Total execution time: 3.3 seconds
```
> Here we defined both  the starting and ending date and time of the clean up period. 


Another way, clean `QualityControl` objects over 1 day with a limit object transaction deletion to 50000. 

```bash
$> sc-cleaner -v -d postgresql://user:password@hostname/seiscomp3 -t qualitycontrol --limit 50000 -st 2019-12-03 00:00:00 --day 1
```

```
2019-12-11 00:59:01 [START] Cleaning Up Seiscomp3 Database
2019-12-11 00:59:01 [INFO] Objects targeted : ['qualitycontrol']
2019-12-11 00:59:01 [INFO] Max number of deleted object in a transaction : 50000 elements
2019-12-11 00:59:01 [INFO] Remove targeted unassociated elements found between
				[2019-12-03 00:00:00] - [2019-12-04 00:00:00]
2019-12-11 00:59:01 [INFO] Database schema-version : 0.11
2019-12-11 00:59:01 [INFO] Clean up QCLog objects
2019-12-11 00:59:01 [INFO] ...  query 1 : 0 obj deleted in 0.00 s
2019-12-11 00:59:01 [INFO] 1 queries executed
2019-12-11 00:59:01 [INFO] 0 QCLog deleted in 0.01 seconds
2019-12-11 00:59:01 [INFO] Clean up Outage objects
2019-12-11 00:59:02 [INFO] ...  query 1 : 32 obj deleted in 0.97 s
2019-12-11 00:59:02 [INFO] 1 queries executed
2019-12-11 00:59:02 [INFO] 32 Outage deleted in 0.97 seconds
2019-12-11 00:59:02 [INFO] Clean up WaveformQuality objects
2019-12-11 00:59:35 [INFO] ...  query 1 : 50000 obj deleted in 32.37 s
2019-12-11 00:59:51 [INFO] ...  query 2 : 25675 obj deleted in 16.37 s
2019-12-11 00:59:51 [INFO] 2 queries executed
2019-12-11 00:59:51 [INFO] 75675 WaveformQuality deleted in 48.74 seconds
2019-12-11 00:59:51 [INFO] Total execution time: 49.8 seconds
```

> qualitycontrol clean-up is necessary if `scqc` has been enabled, indeed the service build an average of 3500 objects in one hour for ~ 400 streams.
> These objects increase rapidly the size of database.


> **Note :**
>
> In case of applying the sc-cleaner on a un-cleaned seiscomp3 database (> 1 year) , be aware that it will take a very long time to delete all Waveformquality objects.
> In this case we advise to lower down the limit of transaction to 10 000 and clean the database step by step.


An other example , using the command-line option `--vacuum`, after deleting the objects it aplied a 'VACCUM FULL ANALYSE target;'.
This will get back some free disk space especially from the table waveformquality.

```
$> sc-cleaner -v -d postgresql://user:password@hostname/seiscomp3 -t qualitycontrol --limit 50000 -st 2019-11-13T00:00:00 -ed 2019-11-14T00:00:00 --vacuum
```

```
2019-12-12 03:38:14 [INFO] Objects targeted : ['qualitycontrol']
2019-12-12 03:38:14 [INFO] Max number of deleted object in a transaction : 50000 elements
2019-12-12 03:38:14 [INFO] Remove targeted unassociated elements found between
				[2019-11-13 00:00:00] - [2019-11-14 00:00:00]
2019-12-12 03:38:14 [INFO] Database schema-version : 0.11
2019-12-12 03:38:14 [INFO] Clean up QCLog objects
2019-12-12 03:38:14 [INFO] ...  query 1 : 0 obj deleted in 0.00 s
2019-12-12 03:38:14 [INFO] 1 queries executed
2019-12-12 03:38:14 [INFO] 0 QCLog deleted in 0.00 seconds
2019-12-12 03:38:14 [INFO] Clean up Outage objects
2019-12-12 03:38:14 [INFO] ...  query 1 : 21 obj deleted in 0.29 s
2019-12-12 03:38:14 [INFO] 1 queries executed
2019-12-12 03:38:14 [INFO] Execute : VACUUM FULL ANALYSE VERBOSE outage
2019-12-12 03:38:14 [INFO] INFO:  vacuuming "public.outage"
2019-12-12 03:38:14 [INFO] INFO:  "outage": found 21 removable, 904 nonremovable row versions in 18 pages
DETAIL:  0 dead row versions cannot be removed yet.
CPU 0.00s/0.00u sec elapsed 0.00 sec.
2019-12-12 03:38:14 [INFO] INFO:  analyzing "public.outage"
2019-12-12 03:38:14 [INFO] INFO:  "outage": scanned 17 of 17 pages, containing 904 live rows and 0 dead rows; 904 rows in sample, 904 estimated total rows
2019-12-12 03:38:14 [INFO] 21 Outage deleted in 0.32 seconds
2019-12-12 03:38:14 [INFO] Clean up WaveformQuality objects
2019-12-12 03:38:46 [INFO] ...  query 1 : 50000 obj deleted in 31.63 s
2019-12-12 03:39:05 [INFO] ...  query 2 : 26669 obj deleted in 19.73 s
2019-12-12 03:39:05 [INFO] 2 queries executed
2019-12-12 03:39:05 [INFO] Execute : VACUUM FULL ANALYSE VERBOSE waveformquality
2019-12-12 03:39:36 [INFO] INFO:  vacuuming "public.outage"
2019-12-12 03:39:36 [INFO] INFO:  "outage": found 21 removable, 904 nonremovable row versions in 18 pages
DETAIL:  0 dead row versions cannot be removed yet.
CPU 0.00s/0.00u sec elapsed 0.00 sec.
2019-12-12 03:39:36 [INFO] INFO:  analyzing "public.outage"
2019-12-12 03:39:36 [INFO] INFO:  "outage": scanned 17 of 17 pages, containing 904 live rows and 0 dead rows; 904 rows in sample, 904 estimated total rows
2019-12-12 03:39:36 [INFO] INFO:  vacuuming "public.waveformquality"
2019-12-12 03:39:36 [INFO] INFO:  "waveformquality": found 26743 removable, 2341018 nonremovable row versions in 62045 pages
DETAIL:  0 dead row versions cannot be removed yet.
CPU 0.48s/1.30u sec elapsed 2.57 sec.
2019-12-12 03:39:36 [INFO] INFO:  analyzing "public.waveformquality"
2019-12-12 03:39:36 [INFO] INFO:  "waveformquality": scanned 30000 of 58187 pages, containing 1206999 live rows and 9 dead rows; 30000 rows in sample, 2341037 estimated total rows
2019-12-12 03:39:36 [INFO] 76669 WaveformQuality deleted in 82.18 seconds
2019-12-12 03:39:36 [INFO] Total execution time: 82.5 seconds

```  



Using `Docker`
--------------

It is possible to use `Docker` to run `sc-cleaner`:

```bash
$> docker build -t sc-cleaner .
$> docker run sc-cleaner -d postgresql://user:password@hostname/database
```


Installation
------------

See file `requirements.txt`, here an example of how to install requirements.

```bash
$> yum install python3-pip python3-setuptools
$> pip3 install --upgrade pip
$> pip3 install peewee
$> pip3 install psycopg2
```

> **Careful**
>
> sc-cleaner is a python3 package, depending the OS python2.7 can be 
> the default python distribution.
> Be aware to use python3 environnement for installation and future use.

You also need to check if the following environnement var **PYTHONPATH** 
is correctly defined to the python3+ distribution

```bash
$> export PYTHONPATH=/usr/lib64/python3.x/site-packages:/usr/lib/python3.x/site-packages:~/.local/lib/python3.x/site-packages:$PYTHONPATH
```

Download and install the package

```bash
$> git clone git@gitlab.com:eost/sc-cleaner.git
$> cd sc-cleaner
```

Then execute as root to install sc-cleaner (usually installed in /usr/bin or usr/local/bin).

```bash
$> sudo python3.x ./setup.py install
$> sudo easy_install-3.x dist/sc_cleaner-1.2-py3.x.egg
```

or as user for a local install, usefull for user without root access ( please update your PYTHONPATH as recommended before)

```bash
$> python3 ./setup.py install --prefix /home/$USER/.local
```

The tool sc-cleaner is now installed under your home directory `~/.local/bin/sc-cleaner`.

> ** NOTE**
> Update your environnement path `$PATH` if needed.  


