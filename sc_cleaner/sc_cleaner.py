#!/usr/bin/env python3

import argparse
from datetime import datetime, timedelta
import logging
import os
import sys
import time
import urllib.parse

from peewee import (JOIN, MySQLDatabase, NodeList,
                    PostgresqlDatabase, SQL)

from .models import base_models
from .models import postgresql_models
from .models import mysql_models


logger = logging.getLogger(__name__)


MODELS = {
    'postgresql': postgresql_models,
    'mysql': mysql_models,
}


# Return database class following dbms
DBMS = {
    'postgresql': PostgresqlDatabase,
    'mysql': MySQLDatabase,
}


def standardize_models(models):
    """
    Fix some attributes in the models.

    For instance, set Amplitude pickid to Amplitude.pickid.
    (Postgresql models: Amplitude.m_pickid, Mysql models: Amplitude.pickid)
    """
    Amplitude = models.Amplitude
    Arrival = models.Arrival
    Originreference = models.Originreference
    Publicobject = models.Publicobject
    Stationmagnitude = models.Stationmagnitude

    if not hasattr(Amplitude, 'pickid'):
        Amplitude.pickid = Amplitude.m_pickid
    if not hasattr(Arrival, 'pickid'):
        Arrival.pickid = Arrival.m_pickid
    if not hasattr(Originreference, 'originid'):
        Originreference.originid = Originreference.m_originid
    if not hasattr(Publicobject, 'publicid'):
        Publicobject.publicid = Publicobject.m_publicid
    if not hasattr(Stationmagnitude, 'amplitudeid'):
        Stationmagnitude.amplitudeid = Stationmagnitude.m_amplitudeid


def get_database(db_url):
    """
    Create database object from the database url.

    Return a tuple with the database and the dbms used.
    """
    result = urllib.parse.urlparse(db_url)
    dbms = result.scheme
    user = result.username or ''
    password = result.password or ''
    db_name = result.path[1:]
    hostname = result.hostname or ''
    port = result.port or ''

    database = DBMS[dbms](db_name, user=user, password=password,
                          host=hostname, port=port)

    return database, dbms


class DatabaseCleaner():
    """
    Clean up Seiscomp database.
    """
    VERSION = '0.11'

    def __init__(self, dbconnect, models, targets, limit, time_limit,time_limit_inf=None,
                 is_mysql_database=False,isolation_level=1):
        # Database connection object
        self.db = dbconnect
        self.cursor = self.db.cursor()
        # Database models
        self.models = models
        # Object to remove
        self.targets = targets
        # Fix a limit for a transaction
        self.limit = limit
        self.time_limit = time_limit
        self.time_limit_inf = time_limit_inf
        #
        self.is_mysql_database = is_mysql_database
        #
        self.start_object = time.time()
        self.start_query = time.time()
        # Default 1
        self.isolation_level = isolation_level

    def check_version(self):
        """
        Print a warning if the database version is not the good one.
        """
        Meta = self.models.Meta

        schema_version = (Meta
                          .select(Meta.value)
                          .where(Meta.name == 'Schema-Version'))
        try:
            version = schema_version.get().value
            if version != DatabaseCleaner.VERSION:
                logger.warning("The database is using the schema %s but the "
                               "script works with schema %s, be careful!"
                               % (version, DatabaseCleaner.VERSION))
            else:
                logger.info("Database schema-version : %s" % version)
        except AttributeError:
            logger.warning("The database schema isn't specified. This script "
                           "works with schema %s, be careful!"
                           % DatabaseCleaner.VERSION)
    def vacuum(self, table):
        """
        Perform and execute a vaccum full analyse of the cleaned table
        Do it only if there was deleted objects
        """
        try:
            query =("VACUUM FULL ANALYSE VERBOSE %s" % table)
            logger.info('Execute : %s' % query)
            self.cursor.execute(query)
            self.db.commit()
            for msg in self.db.notices:
                logger.info('%s' % msg)
        except:
            logger.warning('Something wrong happens while executing query : %s' % query)

    def delete_object(self, objects):
        """
        Delete Objects (not Picks or Amplitudes). This way, other Object
        referenced with ForeignKey will be deleted too.
        """
        self.start_object=time.time()
        Object = self.models.Object

        deleted_object_nb = 0
        count = self.limit

        # TODO : Try a query without a limit in object to evaluate 
        # the execution time without limit
        # it seems that the query.execute() tme is more dependant of the total
        # amount of object available in the time window.
        #
        # So Perhaps it could be better to use incremntal timewindow in sql request
        
        
        # Add the limit to the transaction
        objects = objects.limit(self.limit)
        
        # MySQL can't use LIMIT with IN…
        if self.models == mysql_models:
            objects = NodeList((SQL("SELECT * FROM"), objects, SQL("AS tmp")))

        query = Object.delete().where(Object._oid.in_(objects))
        nquery=0
        while count == self.limit:
            self.start_query=time.time()
            count = query.execute()
            deleted_object_nb += count
            nquery+=1
            logger.info('...  query %d : %d obj deleted in %.2f s' % (nquery,count,(time.time() - self.start_query)))

        logger.info('%d queries executed' % nquery)

        return deleted_object_nb

    def clean_amplitude(self):
        """
        Remove amplitudes not referenced in Stationmagnitude without a
        Pick referenced in Arrival.
        """
        logger.info('Clean up Amplitude')

        Amplitude = self.models.Amplitude
        Arrival = self.models.Arrival
        Publicobject = self.models.Publicobject
        Stationmagnitude = self.models.Stationmagnitude

        if self.time_limit_inf is None:
            # Select Amplitudes older than the time limit
            amplitudes = (Amplitude
                          .select(Amplitude._oid)
                          .where(Amplitude._last_modified < self.time_limit))
        else:
            # Select Amplitudes modified between time range
            amplitudes = (Amplitude
                          .select(Amplitude._oid)
                          .where(Amplitude._last_modified < self.time_limit)
                          .where(Amplitude._last_modified > self.time_limit_inf))

        # Select Amplitudes not referenced in StationMagnitudes
        predicate = Stationmagnitude.amplitudeid == Publicobject.publicid
        amplitudes_not_ref = \
            (amplitudes
             .distinct()
             .join(Publicobject, on=(Publicobject._oid == Amplitude._oid))
             .join(Stationmagnitude, JOIN.LEFT_OUTER, on=predicate)
             .where(Stationmagnitude.amplitudeid.is_null(True)))

        # Select Amplitude with Pick not referenced in Arrival
        arrival_predicate = Arrival.pickid == Amplitude.pickid
        amplitudes_lost = \
            (amplitudes_not_ref
             .join(Arrival, JOIN.LEFT_OUTER, on=arrival_predicate)
             .where(Arrival.pickid.is_null(True)))

        count = self.delete_object(amplitudes_lost)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('amplitude')

        logger.info('%s Amplitudes deleted in %.2f seconds' % (count,(time.time() - self.start_object)))

    def clean_pick(self):
        """
        Remove Picks not referenced in Arrival nor Amplitude
        """
        logger.info('Clean up Pick')

        Amplitude = self.models.Amplitude
        Arrival = self.models.Arrival
        Pick = self.models.Pick
        Publicobject = self.models.Publicobject

        if self.time_limit_inf is None:
            # Select Picks older than the time limit
            logger.debug("Selecting Picks older than the time limit")
            picks = (Pick
                     .select(Pick._oid)
                     .where(Pick._last_modified < self.time_limit))
        else:
            # Select Picks modified in the time range
            logger.debug("Selecting Picks in the the defined time range")
            picks = (Pick
                     .select(Pick._oid)
                     .where(Pick._last_modified < self.time_limit)
                     .where(Pick._last_modified > self.time_limit_inf))
             
        # Select Picks not referenced in Arrivals
        arrival_predicate = Arrival.pickid == Publicobject.publicid
        picks_no_arrival = \
            (picks
             .distinct()
             .join(Publicobject, on=(Publicobject._oid == Pick._oid))
             .join(Arrival, JOIN.LEFT_OUTER, on=arrival_predicate)
             .where(Arrival.pickid.is_null(True)))

        
        # Select Picks not referenced in Amplitudes
        amplitude_predicate = Amplitude.pickid == Publicobject.publicid
        picks_lost = (picks_no_arrival
                      .join(Amplitude, JOIN.LEFT_OUTER, on=amplitude_predicate)
                      .where(Amplitude.pickid.is_null(True)))

        count = self.delete_object(picks_lost)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('pick')

        logger.info('%s Picks deleted in %.2f seconds' % (count,(time.time() - self.start_object)))


    def clean_origin(self):
        """
        Remove unassociated Origin
        """
        logger.info('Clean up Origin')
        Origin = self.models.Origin
        Originreference = self.models.Originreference
        Publicobject = self.models.Publicobject
        Magnitude = self.models.Magnitude
        Stationmagnitude = self.models.Stationmagnitude
        Stationmagnitudecontribution = self.models.Stationmagnitudecontribution
        Arrival = self.models.Arrival

        if self.time_limit_inf is None:
            # Select origins and originReferences older than the time limit
            origins = \
                (Origin
                 .select(Origin._oid)
                 .where(Origin._last_modified < self.time_limit))
        else:
            # Select origins originReferences in the time range
            origins = \
                (Origin
                 .select(Origin._oid)
                 .where(Origin._last_modified < self.time_limit)
                 .where(Origin._last_modified > self.time_limit_inf))

        # Select Origin without Origin reference
        predicate = Originreference.originid == Publicobject.publicid
        origins_not_asso = \
            (origins
             .distinct()
             .join(Publicobject, on=(Publicobject._oid == Origin._oid))
             .join(Originreference, JOIN.LEFT_OUTER, on=predicate)
             .where(Originreference.originid.is_null(True)) )

        logger.info('Found %d unassociated Origin' % len(origins_not_asso) )
        count=0
        if len(origins_not_asso) > 0:
            # For each unassociated origin we look for any related objects
            for org in origins_not_asso:
                logger.debug("Found unassociated orgin with _oid : %s" % org._oid)

                # Search for arrival associated arrivals
                arrival_lost = \
                    (Arrival
                     .select(Arrival._oid)
                     .where(Arrival._parent_oid == org._oid) )
                if len(arrival_lost) > 0:
                    logger.debug("     -> Arrival : %d objects found" % len(arrival_lost) )
                    # removing arrivals
                    count += self.delete_object(arrival_lost)

                # Search for associated magnitudes
                magnitudes_lost = \
                    (Magnitude
                     .select(Magnitude._oid)
                     .where(Magnitude._parent_oid == org._oid) )
                if len(magnitudes_lost) == 0:
                    continue

                # Search for station magnitude contribution
                for mag in magnitudes_lost:
                    sta_mag_contrib_lost = \
                        (Stationmagnitudecontribution
                         .select(Stationmagnitudecontribution._oid)
                         .where(Stationmagnitudecontribution._parent_oid == mag._oid) )
                    if len(sta_mag_contrib_lost) > 0:
                        logger.debug("     -> StationMagContrib : %d objects found"
                                     % len(sta_mag_contrib_lost) )
                        count += self.delete_object(sta_mag_contrib_lost)
                logger.debug("  -> Magnitude : %d objects found" % len(magnitudes_lost) )
                count += self.delete_object(magnitudes_lost)

                # Search for station magnitude
                stationmagnitudes_lost = \
                    (Stationmagnitude
                     .select(Stationmagnitude._oid)
                     .where(Stationmagnitude._parent_oid == org._oid) )
                if len(stationmagnitudes_lost) > 0:
                    logger.debug("  -> StationMagnitude : %d objects found"
                                 % len(stationmagnitudes_lost) )
                    count += self.delete_object(stationmagnitudes_lost)

            # Delete unassociated origin objects
            logger.info('Found %d unassociated Origin' % len(origins_not_asso) )
            count += self.delete_object(origins_not_asso)
            logger.info('%s objects deleted in %.2f seconds' % (count,(time.time() - self.start_object)))
            # Then automaticaly removed amplitudes and picks
            self.clean_amplitude()
            self.clean_pick()


    def clean_originreference(self):
        """
        Remove Originreferences without reference to an existing Origin
        """
        logger.info('Clean up Originreference')

        Originreference = self.models.Originreference
        Publicobject = self.models.Publicobject

        if self.time_limit_inf is None:
            # Select originReferences older than the time limit
            originreferences = \
                               (Originreference
                                .select(Originreference._oid)
                                .where(Originreference._last_modified < self.time_limit))
        else:
            # Select originReferences in the time range
            originreferences = \
                               (Originreference
                                .select(Originreference._oid)
                                .where(Originreference._last_modified < self.time_limit)
                                .where(Originreference._last_modified > self.time_limit_inf))

        # Select Originreferences without Origin reference
        predicate = Originreference.originid == Publicobject.publicid
        originreferences_lost = \
            (originreferences
             .join(Publicobject, JOIN.LEFT_OUTER, on=predicate)
             .where(Publicobject.publicid.is_null(True)))

        count = self.delete_object(originreferences_lost)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('originreference')

        logger.info('%s Originreferences deleted in %.2f seconds' % (count,(time.time() - self.start_object)))

    def clean_qualitycontrol(self):
        """
        Remove QualityControl objects older than the specified timestamp or in the given time range
           * Qclog
           * Outage
           * Waveformquality
        """
        #-------
        logger.info('Clean up QCLog objects')
        QCLog = self.models.Qclog
        
        if self.time_limit_inf is None:
            # Select QCLog older than the time limit
            logger.debug("Selecting 'QCLog' older than the time limit")
            qclogs = (QCLog
                     .select(QCLog._oid)
                     .where(QCLog._last_modified < self.time_limit))
        else:
            # Select QCLog modified in the time range
            logger.debug("Selecting 'QCLog' in the defined time range limit")
            qclogs = (QCLog
                     .select(QCLog._oid)
                     .where(QCLog._last_modified < self.time_limit)
                     .where(QCLog._last_modified > self.time_limit_inf))

        count = self.delete_object(qclogs)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('qclog')

        logger.info('%s QCLog deleted in %.2f seconds' % (count,(time.time() - self.start_object)))
        #-------
        logger.info('Clean up Outage objects')
        Outage = self.models.Outage

        if self.time_limit_inf is None:
            # Select Outage older than the time limit
            logger.debug("Selecting 'Outage' older than the time limit")
            outageqc = (Outage
                     .select(Outage._oid)
                     .where(Outage._last_modified < self.time_limit))
        else:
            # Select Outage modified in the time range
            logger.debug("Selecting 'Outage' in the defined time range limit")
            outageqc = (Outage
                     .select(Outage._oid)
                     .where(Outage._last_modified < self.time_limit)
                     .where(Outage._last_modified > self.time_limit_inf))

        count = self.delete_object(outageqc)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('outage')

        logger.info('%s Outage deleted in %.2f seconds' % (count,(time.time() - self.start_object)))
        #-------
        logger.info('Clean up WaveformQuality objects')
        Waveformquality = self.models.Waveformquality

        if self.time_limit_inf is None:
            # Select WaveformQuality older than the time limit
            logger.debug("Selecting 'WaveformQuality' older than the time limit")
            waveqc = (Waveformquality
                     .select(Waveformquality._oid)
                     .where(Waveformquality._last_modified < self.time_limit))
        else:
            # Select WaveformQuality modified in the time range
            logger.debug("Selecting 'WaveformQuality' in the defined time range limit")
            waveqc = (Waveformquality
                     .select(Waveformquality._oid)
                     .where(Waveformquality._last_modified < self.time_limit)
                     .where(Waveformquality._last_modified > self.time_limit_inf))

        count = self.delete_object(waveqc)

        if self.isolation_level == 0 and count > 0:
            self.vacuum('waveformquality')

        logger.info('%s WaveformQuality deleted in %.2f seconds' % (count,(time.time() - self.start_object)))

    def clean_object(self):
        """
        Remove object not referenced in other table (except PublicObject)
        """
        logger.info('Clean up Object')

        Object = self.models.Object

        tables = [
            self.models.Access,
            self.models.Amplitude,
            self.models.Amplitudereference,
            self.models.Arclinkrequest,
            self.models.Arclinkrequestline,
            self.models.Arclinkstatusline,
            self.models.Arclinkuser,
            self.models.Arrival,
            self.models.Auxdevice,
            self.models.Auxsource,
            self.models.Auxstream,
            self.models.Comment,
            self.models.Compositetime,
            self.models.Configmodule,
            self.models.Configstation,
            self.models.Dataattributeextent,
            self.models.Dataextent,
            self.models.Datalogger,
            self.models.Dataloggercalibration,
            self.models.Datasegment,
            self.models.Dataused,
            self.models.Decimation,
            self.models.Event,
            self.models.Eventdescription,
            self.models.Focalmechanism,
            self.models.Focalmechanismreference,
            self.models.Journalentry,
            self.models.Magnitude,
            self.models.Momenttensor,
            self.models.Momenttensorcomponentcontribution,
            self.models.Momenttensorphasesetting,
            self.models.Momenttensorstationcontribution,
            self.models.Network,
            self.models.Origin,
            self.models.Originreference,
            self.models.Outage,
            self.models.Parameter,
            self.models.Parameterset,
            self.models.Pick,
            self.models.Pickreference,
            self.models.Qclog,
            self.models.Reading,
            self.models.Responsefap,
            self.models.Responsefir,
            self.models.Responseiir,
            self.models.Responsepaz,
            self.models.Responsepolynomial,
            self.models.Route,
            self.models.Routearclink,
            self.models.Routeseedlink,
            self.models.Sensor,
            self.models.Sensorcalibration,
            self.models.Sensorlocation,
            self.models.Setup,
            self.models.Station,
            self.models.Stationgroup,
            self.models.Stationmagnitude,
            self.models.Stationmagnitudecontribution,
            self.models.Stationreference,
            self.models.Stream,
            self.models.Waveformquality,
        ]

        if self.time_limit_inf is None:
            # Select objects older than the time limit
            objects = (Object
                       .select(Object._oid)
                       .where(Object._timestamp < self.time_limit))
        else:
            # Select objects found in the time range
            objects = (Object
                       .select(Object._oid)
                       .where(Object._timestamp < self.time_limit)
                       .where(Object._timestamp > self.time_limit_inf))

        # Add a condition for each table
        for table in tables:
            t = (table
                 .select(table._oid)
                 .where(table._oid == Object._oid))

            objects = objects.where(NodeList((SQL('NOT EXISTS'), t)))

        # Don't remove 7 first Object._oid!
        # They are only in Object and Publicobject tables and they are:
        # 1 - EventParameters
        # 2 - Config
        # 3 - QualityControl
        # 4 - Inventory
        # 5 - Routing
        # 6 - Journaling
        # 7 - Arclinklog
        objects = objects.where(Object._oid > 7)

        count = self.delete_object(objects)
        
        if self.isolation_level == 0 and count > 0:
            self.vacuum('object')

        logger.info('%s Object deleted in %.2f seconds' % (count,(time.time() - self.start_object)))

    def clean(self):
        """
        Remove unused objects from the database
        """
        # /!\ Always clean up Amplitude BEFORE Pick
        if 'amplitude' in self.targets:
            self.clean_amplitude()
        if 'pick' in self.targets:
            self.clean_pick()
        if 'origin' in self.targets:
            self.clean_origin()
        if 'originreference' in self.targets:
            self.clean_originreference()
        if 'object' in self.targets:
            self.clean_object()
        if 'qualitycontrol' in self.targets:
            self.clean_qualitycontrol()

def main():
    parser = argparse.ArgumentParser(description='Clean up seiscomp database')
    parser.add_argument('-d', '--database', help='Database to clean up')
    parser.add_argument('--limit',
                        help='Max number of deleted elements in a transaction',
                        default=10000, type=int)
    parser.add_argument('-v', '--verbose',
                        help=('Can be supplied multiple time to increase '
                              'verbosity (default: WARNING).'),
                        action='count', default=0)
    parser.add_argument('--day', help="Number of days to keep intact, or time window in days when using with `--starttime` or `--endtime`",
                        default=30, type=int)
    parser.add_argument('-st','--starttime', help="Start timestamp to clean-up database")
    parser.add_argument('-ed','--endtime', help="End timestamp to clean-up database")
    parser.add_argument('-t', '--target',
                        help=('Choose the object to remove'),
                        choices=['amplitude', 'pick', 'origin', 'originreference',
                                 'object','qualitycontrol'],
                        nargs='+',
                        default=['amplitude', 'pick', 'originreference'])
    parser.add_argument('--vacuum',
                        help='Allow to execute a full vacuum to free disk space',
                        action='count',default=0)

    args = parser.parse_args()
    starttime=args.starttime
    endtime=args.endtime
    db_url = args.database
    limit = args.limit
    verbose = args.verbose
    day_nb = args.day
    targets = args.target
    # Define the isolation_level for commit allowing full vacuum query
    if args.vacuum == 1:
        vacuum = 0
    else: 
        vacuum = 1
    # Set logging level
    levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    log_level = levels[min(len(levels) - 1, verbose)]
    #log_format = '%(asctime)s %(name)s: %(levelname)s: %(message)s'
    log_format = '%(asctime)s [%(levelname)s] %(message)s'
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt="%Y-%m-%d %H:%M:%S")

    # Récupération du nom de la base de données dans une variable
    # d'environnement
    if db_url is None:
        try:
            db_url = os.environ['DATABASE']
        except KeyError:
            logger.error("No specified database, the environment variable "
                         "`database` does not exist.")
            sys.exit(0)

    # Connect to database
    try:
        database, dbms = get_database(db_url)
        base_models.database_proxy.initialize(database)
        dbconnect=database.connection()
        dbconnect.set_isolation_level(vacuum)
    except KeyError:
        logger.error("No connection to database `%s` " % db_url)
        sys.exit(0)

    # Use models corresponding to the database
    models = MODELS[dbms]
    standardize_models(models)

    # Build time_limit depending on commandline parameters
    # Default only one time_limit is used
    time_limit_inf=None
    if starttime is None:
        if endtime is None:
            # DEFAULT MODE
            # Remove only data older than 1 month from now (default) or data older than --day from now
            time_limit = datetime.now() - timedelta(days=day_nb)
        else:
            # Remove only data older than 1 month from 
            try:
                time_limit = datetime.strptime(endtime,"%Y-%m-%dT%H:%M:%S")
                time_limit_inf = time_limit - timedelta(days=day_nb)
            except ValueError:
                logger.error("`--endtime` option '%s' does not match format '%%Y-%%m-%%dT%%H:%%M:%%S'" % endtime)
                sys.exit(0)
    else:
        if endtime is None:
            # Remove only data betwwen starttime and startime + day (default 30)
            try:
                time_limit_inf = datetime.strptime(starttime,"%Y-%m-%dT%H:%M:%S")
                time_limit = time_limit_inf + timedelta(days=day_nb)
            except ValueError:
                logger.error("`--starttime` option '%s' does not match format '%%Y-%%m-%%dT%%H:%%M:%%S'" % starttime)
                sys.exit(0)
        else:
            # Remove only data found between the two specified starttime and endtime
            # In this case, the option --day is not necessary
            try:
                time_limit = datetime.strptime(endtime,"%Y-%m-%dT%H:%M:%S")
                time_limit_inf=datetime.strptime(starttime,"%Y-%m-%dT%H:%M:%S")
                delta=time_limit-time_limit_inf
                # Check if startime is < endtime
                if delta.total_seconds() <0:
                    logger.error("wrong parameters '--starttime' is older than '--endtime'")
                    sys.exit(0)
            except ValueError:
                logger.error("`-st` and/or `-ed' options does not match format '%%Y-%%m-%%dT%%H:%%M:%%S'")
                sys.exit(0)

    logger.info("Objects targeted : %s" % targets[:])
    logger.info("Max number of deleted object in a transaction : %d elements" % limit)
    if time_limit_inf is None:
        logger.info("Keep database unchanged for the last %d days" % day_nb)
        logger.info("Remove targeted unassociated elements found before [%s]" % time_limit)
    else:
        logger.info("Remove targeted unassociated elements found between\n\t\t\t\t[%s] - [%s]" % (time_limit_inf, time_limit))

    start_time = time.time()
    database_cleaner = DatabaseCleaner(dbconnect, models, targets, limit, time_limit, time_limit_inf, False, vacuum)
    try:
        database_cleaner.check_version()
        database_cleaner.clean()
    finally:
        database.close()
        logger.debug("Close databse connection")
        logger.info("Total execution time: %.1f seconds" % (time.time() - start_time))

if __name__ == "__main__":
    main()
