"""
Models have been generated with this command line:

sudo python3 -m pwiz --engine mysql database
"""

from peewee import *
from .base_models import BaseModel

class UnknownField(object):
    def __init__(self, *_, **__): pass

class Object(BaseModel):
    _oid = AutoField()
    _timestamp = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])

    class Meta:
        table_name = 'Object'

class Access(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    end = DateTimeField(null=True)
    locationcode = CharField(column_name='locationCode')
    networkcode = CharField(column_name='networkCode')
    start = DateTimeField()
    stationcode = CharField(column_name='stationCode')
    streamcode = CharField(column_name='streamCode')
    user = CharField()

    class Meta:
        table_name = 'Access'
        indexes = (
            (('_parent_oid', 'networkcode', 'stationcode', 'locationcode', 'streamcode', 'user', 'start'), True),
        )

class Amplitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    amplitude_confidencelevel = FloatField(column_name='amplitude_confidenceLevel', null=True)
    amplitude_loweruncertainty = FloatField(column_name='amplitude_lowerUncertainty', null=True)
    amplitude_pdf_probability_content = TextField(null=True)
    amplitude_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    amplitude_pdf_variable_content = TextField(null=True)
    amplitude_uncertainty = FloatField(null=True)
    amplitude_upperuncertainty = FloatField(column_name='amplitude_upperUncertainty', null=True)
    amplitude_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    amplitude_value = FloatField(null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    evaluationmode = CharField(column_name='evaluationMode', null=True)
    filterid = CharField(column_name='filterID', null=True)
    magnitudehint = CharField(column_name='magnitudeHint', null=True)
    methodid = CharField(column_name='methodID', null=True)
    period_confidencelevel = FloatField(column_name='period_confidenceLevel', null=True)
    period_loweruncertainty = FloatField(column_name='period_lowerUncertainty', null=True)
    period_pdf_probability_content = TextField(null=True)
    period_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    period_pdf_variable_content = TextField(null=True)
    period_uncertainty = FloatField(null=True)
    period_upperuncertainty = FloatField(column_name='period_upperUncertainty', null=True)
    period_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    period_value = FloatField(null=True)
    pickid = CharField(column_name='pickID', index=True, null=True)
    scalingtime_confidencelevel = FloatField(column_name='scalingTime_confidenceLevel', null=True)
    scalingtime_loweruncertainty = FloatField(column_name='scalingTime_lowerUncertainty', null=True)
    scalingtime_pdf_probability_content = TextField(column_name='scalingTime_pdf_probability_content', null=True)
    scalingtime_pdf_used = IntegerField(column_name='scalingTime_pdf_used', constraints=[SQL("DEFAULT 0")])
    scalingtime_pdf_variable_content = TextField(column_name='scalingTime_pdf_variable_content', null=True)
    scalingtime_uncertainty = FloatField(column_name='scalingTime_uncertainty', null=True)
    scalingtime_upperuncertainty = FloatField(column_name='scalingTime_upperUncertainty', null=True)
    scalingtime_used = IntegerField(column_name='scalingTime_used', constraints=[SQL("DEFAULT 0")])
    scalingtime_value = DateTimeField(column_name='scalingTime_value', null=True)
    scalingtime_value_ms = IntegerField(column_name='scalingTime_value_ms', null=True)
    snr = FloatField(null=True)
    timewindow_begin = FloatField(column_name='timeWindow_begin', null=True)
    timewindow_end = FloatField(column_name='timeWindow_end', null=True)
    timewindow_reference = DateTimeField(column_name='timeWindow_reference', index=True, null=True)
    timewindow_reference_ms = IntegerField(column_name='timeWindow_reference_ms', index=True, null=True)
    timewindow_used = IntegerField(column_name='timeWindow_used', constraints=[SQL("DEFAULT 0")])
    type = CharField()
    unit = CharField(null=True)
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode', null=True)
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode', null=True)
    waveformid_used = IntegerField(column_name='waveformID_used', constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'Amplitude'

class Amplitudereference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    amplitudeid = CharField(column_name='amplitudeID', index=True)

    class Meta:
        table_name = 'AmplitudeReference'
        indexes = (
            (('_parent_oid', 'amplitudeid'), True),
        )

class Arclinkrequest(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    clientid = CharField(column_name='clientID', null=True)
    clientip = CharField(column_name='clientIP', null=True)
    created = DateTimeField()
    created_ms = IntegerField()
    header = CharField(null=True)
    label = CharField(null=True)
    message = CharField(null=True)
    requestid = CharField(column_name='requestID')
    status = CharField()
    summary_averagetimewindow = IntegerField(column_name='summary_averageTimeWindow', null=True)
    summary_oklinecount = IntegerField(column_name='summary_okLineCount', null=True)
    summary_totallinecount = IntegerField(column_name='summary_totalLineCount', null=True)
    summary_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    type = CharField()
    userid = CharField(column_name='userID')
    userip = CharField(column_name='userIP', null=True)

    class Meta:
        table_name = 'ArclinkRequest'
        indexes = (
            (('_parent_oid', 'created', 'created_ms', 'requestid', 'userid'), True),
        )

class Arclinkrequestline(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    constraints = CharField(null=True)
    end = DateTimeField()
    end_ms = IntegerField()
    netclass = CharField(column_name='netClass', null=True)
    restricted = IntegerField(null=True)
    shared = IntegerField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()
    status_message = CharField(null=True)
    status_size = IntegerField(null=True)
    status_status = CharField()
    status_type = CharField()
    status_volumeid = CharField(column_name='status_volumeID', null=True)
    streamid_channelcode = CharField(column_name='streamID_channelCode', null=True)
    streamid_locationcode = CharField(column_name='streamID_locationCode', null=True)
    streamid_networkcode = CharField(column_name='streamID_networkCode')
    streamid_resourceuri = CharField(column_name='streamID_resourceURI', null=True)
    streamid_stationcode = CharField(column_name='streamID_stationCode')

    class Meta:
        table_name = 'ArclinkRequestLine'
        indexes = (
            (('_parent_oid', 'start', 'start_ms', 'end', 'end_ms', 'streamid_networkcode', 'streamid_stationcode', 'streamid_locationcode', 'streamid_channelcode', 'streamid_resourceuri'), True),
        )

class Arclinkstatusline(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    message = CharField(null=True)
    size = IntegerField(null=True)
    status = CharField()
    type = CharField()
    volumeid = CharField(column_name='volumeID', null=True)

    class Meta:
        table_name = 'ArclinkStatusLine'
        indexes = (
            (('_parent_oid', 'volumeid', 'type', 'status'), True),
        )

class Arclinkuser(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    email = CharField(null=True)
    name = CharField()
    password = CharField(null=True)

    class Meta:
        table_name = 'ArclinkUser'
        indexes = (
            (('_parent_oid', 'name', 'email'), True),
        )

class Arrival(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    azimuth = FloatField(null=True)
    backazimuthresidual = FloatField(column_name='backazimuthResidual', null=True)
    backazimuthused = IntegerField(column_name='backazimuthUsed', null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    distance = FloatField(null=True)
    earthmodelid = CharField(column_name='earthModelID', null=True)
    horizontalslownessresidual = FloatField(column_name='horizontalSlownessResidual', null=True)
    horizontalslownessused = IntegerField(column_name='horizontalSlownessUsed', null=True)
    phase_code = CharField()
    pickid = CharField(column_name='pickID', index=True)
    preliminary = IntegerField(null=True)
    takeoffangle = FloatField(column_name='takeOffAngle', null=True)
    timecorrection = FloatField(column_name='timeCorrection', null=True)
    timeresidual = FloatField(column_name='timeResidual', null=True)
    timeused = IntegerField(column_name='timeUsed', null=True)
    weight = FloatField(null=True)

    class Meta:
        table_name = 'Arrival'
        indexes = (
            (('_parent_oid', 'pickid'), True),
        )

class Auxdevice(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    description = CharField(null=True)
    manufacturer = CharField(null=True)
    model = CharField(null=True)
    name = CharField()
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'AuxDevice'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Auxsource(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    conversion = CharField(null=True)
    description = CharField(null=True)
    name = CharField()
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    sampleratedenominator = IntegerField(column_name='sampleRateDenominator', null=True)
    sampleratenumerator = IntegerField(column_name='sampleRateNumerator', null=True)
    unit = CharField(null=True)

    class Meta:
        table_name = 'AuxSource'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Auxstream(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    code = CharField()
    device = CharField(null=True)
    deviceserialnumber = CharField(column_name='deviceSerialNumber', null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    flags = CharField(null=True)
    format = CharField(null=True)
    restricted = IntegerField(null=True)
    shared = IntegerField(null=True)
    source = CharField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()

    class Meta:
        table_name = 'AuxStream'
        indexes = (
            (('_parent_oid', 'code', 'start', 'start_ms'), True),
        )

class Comment(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    id = CharField(null=True)
    start = DateTimeField(null=True)
    start_ms = IntegerField(null=True)
    text = TextField()

    class Meta:
        table_name = 'Comment'
        indexes = (
            (('_parent_oid', 'id'), True),
        )

class Compositetime(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    day_confidencelevel = FloatField(column_name='day_confidenceLevel', null=True)
    day_loweruncertainty = IntegerField(column_name='day_lowerUncertainty', null=True)
    day_uncertainty = IntegerField(null=True)
    day_upperuncertainty = IntegerField(column_name='day_upperUncertainty', null=True)
    day_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    day_value = IntegerField(null=True)
    hour_confidencelevel = FloatField(column_name='hour_confidenceLevel', null=True)
    hour_loweruncertainty = IntegerField(column_name='hour_lowerUncertainty', null=True)
    hour_uncertainty = IntegerField(null=True)
    hour_upperuncertainty = IntegerField(column_name='hour_upperUncertainty', null=True)
    hour_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    hour_value = IntegerField(null=True)
    minute_confidencelevel = FloatField(column_name='minute_confidenceLevel', null=True)
    minute_loweruncertainty = IntegerField(column_name='minute_lowerUncertainty', null=True)
    minute_uncertainty = IntegerField(null=True)
    minute_upperuncertainty = IntegerField(column_name='minute_upperUncertainty', null=True)
    minute_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    minute_value = IntegerField(null=True)
    month_confidencelevel = FloatField(column_name='month_confidenceLevel', null=True)
    month_loweruncertainty = IntegerField(column_name='month_lowerUncertainty', null=True)
    month_uncertainty = IntegerField(null=True)
    month_upperuncertainty = IntegerField(column_name='month_upperUncertainty', null=True)
    month_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    month_value = IntegerField(null=True)
    second_confidencelevel = FloatField(column_name='second_confidenceLevel', null=True)
    second_loweruncertainty = FloatField(column_name='second_lowerUncertainty', null=True)
    second_pdf_probability_content = TextField(null=True)
    second_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    second_pdf_variable_content = TextField(null=True)
    second_uncertainty = FloatField(null=True)
    second_upperuncertainty = FloatField(column_name='second_upperUncertainty', null=True)
    second_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    second_value = FloatField(null=True)
    year_confidencelevel = FloatField(column_name='year_confidenceLevel', null=True)
    year_loweruncertainty = IntegerField(column_name='year_lowerUncertainty', null=True)
    year_uncertainty = IntegerField(null=True)
    year_upperuncertainty = IntegerField(column_name='year_upperUncertainty', null=True)
    year_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    year_value = IntegerField(null=True)

    class Meta:
        table_name = 'CompositeTime'

class Configmodule(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    enabled = IntegerField()
    name = CharField()
    parametersetid = CharField(column_name='parameterSetID', index=True, null=True)

    class Meta:
        table_name = 'ConfigModule'

class Configstation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    enabled = IntegerField()
    networkcode = CharField(column_name='networkCode')
    stationcode = CharField(column_name='stationCode')

    class Meta:
        table_name = 'ConfigStation'
        indexes = (
            (('_parent_oid', 'networkcode', 'stationcode'), True),
        )

class Dataused(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    componentcount = IntegerField(column_name='componentCount')
    shortestperiod = FloatField(column_name='shortestPeriod', null=True)
    stationcount = IntegerField(column_name='stationCount')
    wavetype = CharField(column_name='waveType')

    class Meta:
        table_name = 'DataUsed'

class Datalogger(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    clockmanufacturer = CharField(column_name='clockManufacturer', null=True)
    clockmodel = CharField(column_name='clockModel', null=True)
    clocktype = CharField(column_name='clockType', null=True)
    description = CharField(null=True)
    digitizermanufacturer = CharField(column_name='digitizerManufacturer', null=True)
    digitizermodel = CharField(column_name='digitizerModel', null=True)
    gain = FloatField(null=True)
    maxclockdrift = FloatField(column_name='maxClockDrift', null=True)
    name = CharField(null=True)
    recordermanufacturer = CharField(column_name='recorderManufacturer', null=True)
    recordermodel = CharField(column_name='recorderModel', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'Datalogger'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Dataloggercalibration(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    channel = IntegerField()
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    serialnumber = CharField(column_name='serialNumber')
    start = DateTimeField()
    start_ms = IntegerField()

    class Meta:
        table_name = 'DataloggerCalibration'
        indexes = (
            (('_parent_oid', 'serialnumber', 'channel', 'start', 'start_ms'), True),
        )

class Decimation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    analoguefilterchain_content = TextField(column_name='analogueFilterChain_content', null=True)
    analoguefilterchain_used = IntegerField(column_name='analogueFilterChain_used', constraints=[SQL("DEFAULT 0")])
    digitalfilterchain_content = TextField(column_name='digitalFilterChain_content', null=True)
    digitalfilterchain_used = IntegerField(column_name='digitalFilterChain_used', constraints=[SQL("DEFAULT 0")])
    sampleratedenominator = IntegerField(column_name='sampleRateDenominator')
    sampleratenumerator = IntegerField(column_name='sampleRateNumerator')

    class Meta:
        table_name = 'Decimation'
        indexes = (
            (('_parent_oid', 'sampleratenumerator', 'sampleratedenominator'), True),
        )

class Event(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    preferredfocalmechanismid = CharField(column_name='preferredFocalMechanismID', index=True, null=True)
    preferredmagnitudeid = CharField(column_name='preferredMagnitudeID', index=True, null=True)
    preferredoriginid = CharField(column_name='preferredOriginID', index=True, null=True)
    type = CharField(null=True)
    typecertainty = CharField(column_name='typeCertainty', null=True)

    class Meta:
        table_name = 'Event'

class Eventdescription(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    text = CharField()
    type = CharField()

    class Meta:
        table_name = 'EventDescription'
        indexes = (
            (('_parent_oid', 'type'), True),
        )

class Focalmechanism(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    azimuthalgap = FloatField(column_name='azimuthalGap', null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    evaluationmode = CharField(column_name='evaluationMode', null=True)
    evaluationstatus = CharField(column_name='evaluationStatus', null=True)
    methodid = CharField(column_name='methodID', null=True)
    misfit = FloatField(null=True)
    nodalplanes_nodalplane1_dip_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane1_dip_confidenceLevel', null=True)
    nodalplanes_nodalplane1_dip_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_dip_lowerUncertainty', null=True)
    nodalplanes_nodalplane1_dip_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane1_dip_pdf_probability_content', null=True)
    nodalplanes_nodalplane1_dip_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane1_dip_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane1_dip_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane1_dip_pdf_variable_content', null=True)
    nodalplanes_nodalplane1_dip_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_dip_uncertainty', null=True)
    nodalplanes_nodalplane1_dip_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_dip_upperUncertainty', null=True)
    nodalplanes_nodalplane1_dip_value = FloatField(column_name='nodalPlanes_nodalPlane1_dip_value', null=True)
    nodalplanes_nodalplane1_rake_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane1_rake_confidenceLevel', null=True)
    nodalplanes_nodalplane1_rake_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_rake_lowerUncertainty', null=True)
    nodalplanes_nodalplane1_rake_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane1_rake_pdf_probability_content', null=True)
    nodalplanes_nodalplane1_rake_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane1_rake_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane1_rake_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane1_rake_pdf_variable_content', null=True)
    nodalplanes_nodalplane1_rake_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_rake_uncertainty', null=True)
    nodalplanes_nodalplane1_rake_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_rake_upperUncertainty', null=True)
    nodalplanes_nodalplane1_rake_value = FloatField(column_name='nodalPlanes_nodalPlane1_rake_value', null=True)
    nodalplanes_nodalplane1_strike_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane1_strike_confidenceLevel', null=True)
    nodalplanes_nodalplane1_strike_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_strike_lowerUncertainty', null=True)
    nodalplanes_nodalplane1_strike_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane1_strike_pdf_probability_content', null=True)
    nodalplanes_nodalplane1_strike_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane1_strike_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane1_strike_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane1_strike_pdf_variable_content', null=True)
    nodalplanes_nodalplane1_strike_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_strike_uncertainty', null=True)
    nodalplanes_nodalplane1_strike_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane1_strike_upperUncertainty', null=True)
    nodalplanes_nodalplane1_strike_value = FloatField(column_name='nodalPlanes_nodalPlane1_strike_value', null=True)
    nodalplanes_nodalplane1_used = IntegerField(column_name='nodalPlanes_nodalPlane1_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane2_dip_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane2_dip_confidenceLevel', null=True)
    nodalplanes_nodalplane2_dip_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_dip_lowerUncertainty', null=True)
    nodalplanes_nodalplane2_dip_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane2_dip_pdf_probability_content', null=True)
    nodalplanes_nodalplane2_dip_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane2_dip_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane2_dip_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane2_dip_pdf_variable_content', null=True)
    nodalplanes_nodalplane2_dip_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_dip_uncertainty', null=True)
    nodalplanes_nodalplane2_dip_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_dip_upperUncertainty', null=True)
    nodalplanes_nodalplane2_dip_value = FloatField(column_name='nodalPlanes_nodalPlane2_dip_value', null=True)
    nodalplanes_nodalplane2_rake_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane2_rake_confidenceLevel', null=True)
    nodalplanes_nodalplane2_rake_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_rake_lowerUncertainty', null=True)
    nodalplanes_nodalplane2_rake_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane2_rake_pdf_probability_content', null=True)
    nodalplanes_nodalplane2_rake_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane2_rake_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane2_rake_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane2_rake_pdf_variable_content', null=True)
    nodalplanes_nodalplane2_rake_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_rake_uncertainty', null=True)
    nodalplanes_nodalplane2_rake_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_rake_upperUncertainty', null=True)
    nodalplanes_nodalplane2_rake_value = FloatField(column_name='nodalPlanes_nodalPlane2_rake_value', null=True)
    nodalplanes_nodalplane2_strike_confidencelevel = FloatField(column_name='nodalPlanes_nodalPlane2_strike_confidenceLevel', null=True)
    nodalplanes_nodalplane2_strike_loweruncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_strike_lowerUncertainty', null=True)
    nodalplanes_nodalplane2_strike_pdf_probability_content = TextField(column_name='nodalPlanes_nodalPlane2_strike_pdf_probability_content', null=True)
    nodalplanes_nodalplane2_strike_pdf_used = IntegerField(column_name='nodalPlanes_nodalPlane2_strike_pdf_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_nodalplane2_strike_pdf_variable_content = TextField(column_name='nodalPlanes_nodalPlane2_strike_pdf_variable_content', null=True)
    nodalplanes_nodalplane2_strike_uncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_strike_uncertainty', null=True)
    nodalplanes_nodalplane2_strike_upperuncertainty = FloatField(column_name='nodalPlanes_nodalPlane2_strike_upperUncertainty', null=True)
    nodalplanes_nodalplane2_strike_value = FloatField(column_name='nodalPlanes_nodalPlane2_strike_value', null=True)
    nodalplanes_nodalplane2_used = IntegerField(column_name='nodalPlanes_nodalPlane2_used', constraints=[SQL("DEFAULT 0")])
    nodalplanes_preferredplane = IntegerField(column_name='nodalPlanes_preferredPlane', null=True)
    nodalplanes_used = IntegerField(column_name='nodalPlanes_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_naxis_azimuth_confidencelevel = FloatField(column_name='principalAxes_nAxis_azimuth_confidenceLevel', null=True)
    principalaxes_naxis_azimuth_loweruncertainty = FloatField(column_name='principalAxes_nAxis_azimuth_lowerUncertainty', null=True)
    principalaxes_naxis_azimuth_pdf_probability_content = TextField(column_name='principalAxes_nAxis_azimuth_pdf_probability_content', null=True)
    principalaxes_naxis_azimuth_pdf_used = IntegerField(column_name='principalAxes_nAxis_azimuth_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_naxis_azimuth_pdf_variable_content = TextField(column_name='principalAxes_nAxis_azimuth_pdf_variable_content', null=True)
    principalaxes_naxis_azimuth_uncertainty = FloatField(column_name='principalAxes_nAxis_azimuth_uncertainty', null=True)
    principalaxes_naxis_azimuth_upperuncertainty = FloatField(column_name='principalAxes_nAxis_azimuth_upperUncertainty', null=True)
    principalaxes_naxis_azimuth_value = FloatField(column_name='principalAxes_nAxis_azimuth_value', null=True)
    principalaxes_naxis_length_confidencelevel = FloatField(column_name='principalAxes_nAxis_length_confidenceLevel', null=True)
    principalaxes_naxis_length_loweruncertainty = FloatField(column_name='principalAxes_nAxis_length_lowerUncertainty', null=True)
    principalaxes_naxis_length_pdf_probability_content = TextField(column_name='principalAxes_nAxis_length_pdf_probability_content', null=True)
    principalaxes_naxis_length_pdf_used = IntegerField(column_name='principalAxes_nAxis_length_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_naxis_length_pdf_variable_content = TextField(column_name='principalAxes_nAxis_length_pdf_variable_content', null=True)
    principalaxes_naxis_length_uncertainty = FloatField(column_name='principalAxes_nAxis_length_uncertainty', null=True)
    principalaxes_naxis_length_upperuncertainty = FloatField(column_name='principalAxes_nAxis_length_upperUncertainty', null=True)
    principalaxes_naxis_length_value = FloatField(column_name='principalAxes_nAxis_length_value', null=True)
    principalaxes_naxis_plunge_confidencelevel = FloatField(column_name='principalAxes_nAxis_plunge_confidenceLevel', null=True)
    principalaxes_naxis_plunge_loweruncertainty = FloatField(column_name='principalAxes_nAxis_plunge_lowerUncertainty', null=True)
    principalaxes_naxis_plunge_pdf_probability_content = TextField(column_name='principalAxes_nAxis_plunge_pdf_probability_content', null=True)
    principalaxes_naxis_plunge_pdf_used = IntegerField(column_name='principalAxes_nAxis_plunge_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_naxis_plunge_pdf_variable_content = TextField(column_name='principalAxes_nAxis_plunge_pdf_variable_content', null=True)
    principalaxes_naxis_plunge_uncertainty = FloatField(column_name='principalAxes_nAxis_plunge_uncertainty', null=True)
    principalaxes_naxis_plunge_upperuncertainty = FloatField(column_name='principalAxes_nAxis_plunge_upperUncertainty', null=True)
    principalaxes_naxis_plunge_value = FloatField(column_name='principalAxes_nAxis_plunge_value', null=True)
    principalaxes_naxis_used = IntegerField(column_name='principalAxes_nAxis_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_paxis_azimuth_confidencelevel = FloatField(column_name='principalAxes_pAxis_azimuth_confidenceLevel', null=True)
    principalaxes_paxis_azimuth_loweruncertainty = FloatField(column_name='principalAxes_pAxis_azimuth_lowerUncertainty', null=True)
    principalaxes_paxis_azimuth_pdf_probability_content = TextField(column_name='principalAxes_pAxis_azimuth_pdf_probability_content', null=True)
    principalaxes_paxis_azimuth_pdf_used = IntegerField(column_name='principalAxes_pAxis_azimuth_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_paxis_azimuth_pdf_variable_content = TextField(column_name='principalAxes_pAxis_azimuth_pdf_variable_content', null=True)
    principalaxes_paxis_azimuth_uncertainty = FloatField(column_name='principalAxes_pAxis_azimuth_uncertainty', null=True)
    principalaxes_paxis_azimuth_upperuncertainty = FloatField(column_name='principalAxes_pAxis_azimuth_upperUncertainty', null=True)
    principalaxes_paxis_azimuth_value = FloatField(column_name='principalAxes_pAxis_azimuth_value', null=True)
    principalaxes_paxis_length_confidencelevel = FloatField(column_name='principalAxes_pAxis_length_confidenceLevel', null=True)
    principalaxes_paxis_length_loweruncertainty = FloatField(column_name='principalAxes_pAxis_length_lowerUncertainty', null=True)
    principalaxes_paxis_length_pdf_probability_content = TextField(column_name='principalAxes_pAxis_length_pdf_probability_content', null=True)
    principalaxes_paxis_length_pdf_used = IntegerField(column_name='principalAxes_pAxis_length_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_paxis_length_pdf_variable_content = TextField(column_name='principalAxes_pAxis_length_pdf_variable_content', null=True)
    principalaxes_paxis_length_uncertainty = FloatField(column_name='principalAxes_pAxis_length_uncertainty', null=True)
    principalaxes_paxis_length_upperuncertainty = FloatField(column_name='principalAxes_pAxis_length_upperUncertainty', null=True)
    principalaxes_paxis_length_value = FloatField(column_name='principalAxes_pAxis_length_value', null=True)
    principalaxes_paxis_plunge_confidencelevel = FloatField(column_name='principalAxes_pAxis_plunge_confidenceLevel', null=True)
    principalaxes_paxis_plunge_loweruncertainty = FloatField(column_name='principalAxes_pAxis_plunge_lowerUncertainty', null=True)
    principalaxes_paxis_plunge_pdf_probability_content = TextField(column_name='principalAxes_pAxis_plunge_pdf_probability_content', null=True)
    principalaxes_paxis_plunge_pdf_used = IntegerField(column_name='principalAxes_pAxis_plunge_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_paxis_plunge_pdf_variable_content = TextField(column_name='principalAxes_pAxis_plunge_pdf_variable_content', null=True)
    principalaxes_paxis_plunge_uncertainty = FloatField(column_name='principalAxes_pAxis_plunge_uncertainty', null=True)
    principalaxes_paxis_plunge_upperuncertainty = FloatField(column_name='principalAxes_pAxis_plunge_upperUncertainty', null=True)
    principalaxes_paxis_plunge_value = FloatField(column_name='principalAxes_pAxis_plunge_value', null=True)
    principalaxes_taxis_azimuth_confidencelevel = FloatField(column_name='principalAxes_tAxis_azimuth_confidenceLevel', null=True)
    principalaxes_taxis_azimuth_loweruncertainty = FloatField(column_name='principalAxes_tAxis_azimuth_lowerUncertainty', null=True)
    principalaxes_taxis_azimuth_pdf_probability_content = TextField(column_name='principalAxes_tAxis_azimuth_pdf_probability_content', null=True)
    principalaxes_taxis_azimuth_pdf_used = IntegerField(column_name='principalAxes_tAxis_azimuth_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_taxis_azimuth_pdf_variable_content = TextField(column_name='principalAxes_tAxis_azimuth_pdf_variable_content', null=True)
    principalaxes_taxis_azimuth_uncertainty = FloatField(column_name='principalAxes_tAxis_azimuth_uncertainty', null=True)
    principalaxes_taxis_azimuth_upperuncertainty = FloatField(column_name='principalAxes_tAxis_azimuth_upperUncertainty', null=True)
    principalaxes_taxis_azimuth_value = FloatField(column_name='principalAxes_tAxis_azimuth_value', null=True)
    principalaxes_taxis_length_confidencelevel = FloatField(column_name='principalAxes_tAxis_length_confidenceLevel', null=True)
    principalaxes_taxis_length_loweruncertainty = FloatField(column_name='principalAxes_tAxis_length_lowerUncertainty', null=True)
    principalaxes_taxis_length_pdf_probability_content = TextField(column_name='principalAxes_tAxis_length_pdf_probability_content', null=True)
    principalaxes_taxis_length_pdf_used = IntegerField(column_name='principalAxes_tAxis_length_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_taxis_length_pdf_variable_content = TextField(column_name='principalAxes_tAxis_length_pdf_variable_content', null=True)
    principalaxes_taxis_length_uncertainty = FloatField(column_name='principalAxes_tAxis_length_uncertainty', null=True)
    principalaxes_taxis_length_upperuncertainty = FloatField(column_name='principalAxes_tAxis_length_upperUncertainty', null=True)
    principalaxes_taxis_length_value = FloatField(column_name='principalAxes_tAxis_length_value', null=True)
    principalaxes_taxis_plunge_confidencelevel = FloatField(column_name='principalAxes_tAxis_plunge_confidenceLevel', null=True)
    principalaxes_taxis_plunge_loweruncertainty = FloatField(column_name='principalAxes_tAxis_plunge_lowerUncertainty', null=True)
    principalaxes_taxis_plunge_pdf_probability_content = TextField(column_name='principalAxes_tAxis_plunge_pdf_probability_content', null=True)
    principalaxes_taxis_plunge_pdf_used = IntegerField(column_name='principalAxes_tAxis_plunge_pdf_used', constraints=[SQL("DEFAULT 0")])
    principalaxes_taxis_plunge_pdf_variable_content = TextField(column_name='principalAxes_tAxis_plunge_pdf_variable_content', null=True)
    principalaxes_taxis_plunge_uncertainty = FloatField(column_name='principalAxes_tAxis_plunge_uncertainty', null=True)
    principalaxes_taxis_plunge_upperuncertainty = FloatField(column_name='principalAxes_tAxis_plunge_upperUncertainty', null=True)
    principalaxes_taxis_plunge_value = FloatField(column_name='principalAxes_tAxis_plunge_value', null=True)
    principalaxes_used = IntegerField(column_name='principalAxes_used', constraints=[SQL("DEFAULT 0")])
    stationdistributionratio = FloatField(column_name='stationDistributionRatio', null=True)
    stationpolaritycount = IntegerField(column_name='stationPolarityCount', null=True)
    triggeringoriginid = CharField(column_name='triggeringOriginID', index=True, null=True)

    class Meta:
        table_name = 'FocalMechanism'

class Focalmechanismreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    focalmechanismid = CharField(column_name='focalMechanismID', index=True)

    class Meta:
        table_name = 'FocalMechanismReference'
        indexes = (
            (('_parent_oid', 'focalmechanismid'), True),
        )

class Journalentry(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    action = CharField()
    created = DateTimeField(null=True)
    created_ms = IntegerField(null=True)
    objectid = CharField(column_name='objectID', index=True)
    parameters = CharField(null=True)
    sender = CharField()

    class Meta:
        table_name = 'JournalEntry'

class Magnitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    azimuthalgap = FloatField(column_name='azimuthalGap', null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    evaluationstatus = CharField(column_name='evaluationStatus', null=True)
    magnitude_confidencelevel = FloatField(column_name='magnitude_confidenceLevel', null=True)
    magnitude_loweruncertainty = FloatField(column_name='magnitude_lowerUncertainty', null=True)
    magnitude_pdf_probability_content = TextField(null=True)
    magnitude_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    magnitude_pdf_variable_content = TextField(null=True)
    magnitude_uncertainty = FloatField(null=True)
    magnitude_upperuncertainty = FloatField(column_name='magnitude_upperUncertainty', null=True)
    magnitude_value = FloatField()
    methodid = CharField(column_name='methodID', null=True)
    originid = CharField(column_name='originID', null=True)
    stationcount = IntegerField(column_name='stationCount', null=True)
    type = CharField(null=True)

    class Meta:
        table_name = 'Magnitude'

class Meta(BaseModel):
    name = CharField(primary_key=True)
    value = CharField()

    class Meta:
        table_name = 'Meta'

class Momenttensor(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    clvd = FloatField(null=True)
    cmtname = CharField(column_name='cmtName', null=True)
    cmtversion = CharField(column_name='cmtVersion', null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    derivedoriginid = CharField(column_name='derivedOriginID', index=True)
    doublecouple = FloatField(column_name='doubleCouple', null=True)
    filterid = CharField(column_name='filterID', null=True)
    greensfunctionid = CharField(column_name='greensFunctionID', null=True)
    iso = FloatField(null=True)
    method = CharField(null=True)
    methodid = CharField(column_name='methodID', null=True)
    momentmagnitudeid = CharField(column_name='momentMagnitudeID', null=True)
    scalarmoment_confidencelevel = FloatField(column_name='scalarMoment_confidenceLevel', null=True)
    scalarmoment_loweruncertainty = FloatField(column_name='scalarMoment_lowerUncertainty', null=True)
    scalarmoment_pdf_probability_content = TextField(column_name='scalarMoment_pdf_probability_content', null=True)
    scalarmoment_pdf_used = IntegerField(column_name='scalarMoment_pdf_used', constraints=[SQL("DEFAULT 0")])
    scalarmoment_pdf_variable_content = TextField(column_name='scalarMoment_pdf_variable_content', null=True)
    scalarmoment_uncertainty = FloatField(column_name='scalarMoment_uncertainty', null=True)
    scalarmoment_upperuncertainty = FloatField(column_name='scalarMoment_upperUncertainty', null=True)
    scalarmoment_used = IntegerField(column_name='scalarMoment_used', constraints=[SQL("DEFAULT 0")])
    scalarmoment_value = FloatField(column_name='scalarMoment_value', null=True)
    sourcetimefunction_decaytime = FloatField(column_name='sourceTimeFunction_decayTime', null=True)
    sourcetimefunction_duration = FloatField(column_name='sourceTimeFunction_duration', null=True)
    sourcetimefunction_risetime = FloatField(column_name='sourceTimeFunction_riseTime', null=True)
    sourcetimefunction_type = CharField(column_name='sourceTimeFunction_type', null=True)
    sourcetimefunction_used = IntegerField(column_name='sourceTimeFunction_used', constraints=[SQL("DEFAULT 0")])
    status = CharField(null=True)
    tensor_mpp_confidencelevel = FloatField(column_name='tensor_Mpp_confidenceLevel', null=True)
    tensor_mpp_loweruncertainty = FloatField(column_name='tensor_Mpp_lowerUncertainty', null=True)
    tensor_mpp_pdf_probability_content = TextField(column_name='tensor_Mpp_pdf_probability_content', null=True)
    tensor_mpp_pdf_used = IntegerField(column_name='tensor_Mpp_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mpp_pdf_variable_content = TextField(column_name='tensor_Mpp_pdf_variable_content', null=True)
    tensor_mpp_uncertainty = FloatField(column_name='tensor_Mpp_uncertainty', null=True)
    tensor_mpp_upperuncertainty = FloatField(column_name='tensor_Mpp_upperUncertainty', null=True)
    tensor_mpp_value = FloatField(column_name='tensor_Mpp_value', null=True)
    tensor_mrp_confidencelevel = FloatField(column_name='tensor_Mrp_confidenceLevel', null=True)
    tensor_mrp_loweruncertainty = FloatField(column_name='tensor_Mrp_lowerUncertainty', null=True)
    tensor_mrp_pdf_probability_content = TextField(column_name='tensor_Mrp_pdf_probability_content', null=True)
    tensor_mrp_pdf_used = IntegerField(column_name='tensor_Mrp_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mrp_pdf_variable_content = TextField(column_name='tensor_Mrp_pdf_variable_content', null=True)
    tensor_mrp_uncertainty = FloatField(column_name='tensor_Mrp_uncertainty', null=True)
    tensor_mrp_upperuncertainty = FloatField(column_name='tensor_Mrp_upperUncertainty', null=True)
    tensor_mrp_value = FloatField(column_name='tensor_Mrp_value', null=True)
    tensor_mrr_confidencelevel = FloatField(column_name='tensor_Mrr_confidenceLevel', null=True)
    tensor_mrr_loweruncertainty = FloatField(column_name='tensor_Mrr_lowerUncertainty', null=True)
    tensor_mrr_pdf_probability_content = TextField(column_name='tensor_Mrr_pdf_probability_content', null=True)
    tensor_mrr_pdf_used = IntegerField(column_name='tensor_Mrr_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mrr_pdf_variable_content = TextField(column_name='tensor_Mrr_pdf_variable_content', null=True)
    tensor_mrr_uncertainty = FloatField(column_name='tensor_Mrr_uncertainty', null=True)
    tensor_mrr_upperuncertainty = FloatField(column_name='tensor_Mrr_upperUncertainty', null=True)
    tensor_mrr_value = FloatField(column_name='tensor_Mrr_value', null=True)
    tensor_mrt_confidencelevel = FloatField(column_name='tensor_Mrt_confidenceLevel', null=True)
    tensor_mrt_loweruncertainty = FloatField(column_name='tensor_Mrt_lowerUncertainty', null=True)
    tensor_mrt_pdf_probability_content = TextField(column_name='tensor_Mrt_pdf_probability_content', null=True)
    tensor_mrt_pdf_used = IntegerField(column_name='tensor_Mrt_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mrt_pdf_variable_content = TextField(column_name='tensor_Mrt_pdf_variable_content', null=True)
    tensor_mrt_uncertainty = FloatField(column_name='tensor_Mrt_uncertainty', null=True)
    tensor_mrt_upperuncertainty = FloatField(column_name='tensor_Mrt_upperUncertainty', null=True)
    tensor_mrt_value = FloatField(column_name='tensor_Mrt_value', null=True)
    tensor_mtp_confidencelevel = FloatField(column_name='tensor_Mtp_confidenceLevel', null=True)
    tensor_mtp_loweruncertainty = FloatField(column_name='tensor_Mtp_lowerUncertainty', null=True)
    tensor_mtp_pdf_probability_content = TextField(column_name='tensor_Mtp_pdf_probability_content', null=True)
    tensor_mtp_pdf_used = IntegerField(column_name='tensor_Mtp_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mtp_pdf_variable_content = TextField(column_name='tensor_Mtp_pdf_variable_content', null=True)
    tensor_mtp_uncertainty = FloatField(column_name='tensor_Mtp_uncertainty', null=True)
    tensor_mtp_upperuncertainty = FloatField(column_name='tensor_Mtp_upperUncertainty', null=True)
    tensor_mtp_value = FloatField(column_name='tensor_Mtp_value', null=True)
    tensor_mtt_confidencelevel = FloatField(column_name='tensor_Mtt_confidenceLevel', null=True)
    tensor_mtt_loweruncertainty = FloatField(column_name='tensor_Mtt_lowerUncertainty', null=True)
    tensor_mtt_pdf_probability_content = TextField(column_name='tensor_Mtt_pdf_probability_content', null=True)
    tensor_mtt_pdf_used = IntegerField(column_name='tensor_Mtt_pdf_used', constraints=[SQL("DEFAULT 0")])
    tensor_mtt_pdf_variable_content = TextField(column_name='tensor_Mtt_pdf_variable_content', null=True)
    tensor_mtt_uncertainty = FloatField(column_name='tensor_Mtt_uncertainty', null=True)
    tensor_mtt_upperuncertainty = FloatField(column_name='tensor_Mtt_upperUncertainty', null=True)
    tensor_mtt_value = FloatField(column_name='tensor_Mtt_value', null=True)
    tensor_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    variance = FloatField(null=True)
    variancereduction = FloatField(column_name='varianceReduction', null=True)

    class Meta:
        table_name = 'MomentTensor'

class Momenttensorcomponentcontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    active = IntegerField()
    component = IntegerField()
    datatimewindow = TextField(column_name='dataTimeWindow')
    misfit = FloatField(null=True)
    phasecode = CharField(column_name='phaseCode')
    snr = FloatField(null=True)
    timeshift = FloatField(column_name='timeShift')
    weight = FloatField()

    class Meta:
        table_name = 'MomentTensorComponentContribution'
        indexes = (
            (('_parent_oid', 'phasecode', 'component'), True),
        )

class Momenttensorphasesetting(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    code = CharField()
    lowerperiod = FloatField(column_name='lowerPeriod')
    maximumtimeshift = FloatField(column_name='maximumTimeShift', null=True)
    minimumsnr = FloatField(column_name='minimumSNR', null=True)
    upperperiod = FloatField(column_name='upperPeriod')

    class Meta:
        table_name = 'MomentTensorPhaseSetting'
        indexes = (
            (('_parent_oid', 'code'), True),
        )

class Momenttensorstationcontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    active = IntegerField()
    timeshift = FloatField(column_name='timeShift', null=True)
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode', null=True)
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode', null=True)
    waveformid_used = IntegerField(column_name='waveformID_used', constraints=[SQL("DEFAULT 0")])
    weight = FloatField(null=True)

    class Meta:
        table_name = 'MomentTensorStationContribution'

class Network(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    archive = CharField(null=True)
    code = CharField()
    description = CharField(null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    institutions = CharField(null=True)
    netclass = CharField(column_name='netClass', null=True)
    region = CharField(null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    restricted = IntegerField(null=True)
    shared = IntegerField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()
    type = CharField(null=True)

    class Meta:
        table_name = 'Network'
        indexes = (
            (('_parent_oid', 'code', 'start', 'start_ms'), True),
        )

class Origin(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    depthtype = CharField(column_name='depthType', null=True)
    depth_confidencelevel = FloatField(column_name='depth_confidenceLevel', null=True)
    depth_loweruncertainty = FloatField(column_name='depth_lowerUncertainty', null=True)
    depth_pdf_probability_content = TextField(null=True)
    depth_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    depth_pdf_variable_content = TextField(null=True)
    depth_uncertainty = FloatField(null=True)
    depth_upperuncertainty = FloatField(column_name='depth_upperUncertainty', null=True)
    depth_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    depth_value = FloatField(null=True)
    earthmodelid = CharField(column_name='earthModelID', null=True)
    epicenterfixed = IntegerField(column_name='epicenterFixed', null=True)
    evaluationmode = CharField(column_name='evaluationMode', null=True)
    evaluationstatus = CharField(column_name='evaluationStatus', null=True)
    latitude_confidencelevel = FloatField(column_name='latitude_confidenceLevel', null=True)
    latitude_loweruncertainty = FloatField(column_name='latitude_lowerUncertainty', null=True)
    latitude_pdf_probability_content = TextField(null=True)
    latitude_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    latitude_pdf_variable_content = TextField(null=True)
    latitude_uncertainty = FloatField(null=True)
    latitude_upperuncertainty = FloatField(column_name='latitude_upperUncertainty', null=True)
    latitude_value = FloatField()
    longitude_confidencelevel = FloatField(column_name='longitude_confidenceLevel', null=True)
    longitude_loweruncertainty = FloatField(column_name='longitude_lowerUncertainty', null=True)
    longitude_pdf_probability_content = TextField(null=True)
    longitude_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    longitude_pdf_variable_content = TextField(null=True)
    longitude_uncertainty = FloatField(null=True)
    longitude_upperuncertainty = FloatField(column_name='longitude_upperUncertainty', null=True)
    longitude_value = FloatField()
    methodid = CharField(column_name='methodID', null=True)
    quality_associatedphasecount = IntegerField(column_name='quality_associatedPhaseCount', null=True)
    quality_associatedstationcount = IntegerField(column_name='quality_associatedStationCount', null=True)
    quality_azimuthalgap = FloatField(column_name='quality_azimuthalGap', null=True)
    quality_depthphasecount = IntegerField(column_name='quality_depthPhaseCount', null=True)
    quality_groundtruthlevel = CharField(column_name='quality_groundTruthLevel', null=True)
    quality_maximumdistance = FloatField(column_name='quality_maximumDistance', null=True)
    quality_mediandistance = FloatField(column_name='quality_medianDistance', null=True)
    quality_minimumdistance = FloatField(column_name='quality_minimumDistance', null=True)
    quality_secondaryazimuthalgap = FloatField(column_name='quality_secondaryAzimuthalGap', null=True)
    quality_standarderror = FloatField(column_name='quality_standardError', null=True)
    quality_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    quality_usedphasecount = IntegerField(column_name='quality_usedPhaseCount', null=True)
    quality_usedstationcount = IntegerField(column_name='quality_usedStationCount', null=True)
    referencesystemid = CharField(column_name='referenceSystemID', null=True)
    timefixed = IntegerField(column_name='timeFixed', null=True)
    time_confidencelevel = FloatField(column_name='time_confidenceLevel', null=True)
    time_loweruncertainty = FloatField(column_name='time_lowerUncertainty', null=True)
    time_pdf_probability_content = TextField(null=True)
    time_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    time_pdf_variable_content = TextField(null=True)
    time_uncertainty = FloatField(null=True)
    time_upperuncertainty = FloatField(column_name='time_upperUncertainty', null=True)
    time_value = DateTimeField(index=True)
    time_value_ms = IntegerField(index=True)
    type = CharField(null=True)
    uncertainty_azimuthmaxhorizontaluncertainty = FloatField(column_name='uncertainty_azimuthMaxHorizontalUncertainty', null=True)
    uncertainty_confidenceellipsoid_majoraxisazimuth = FloatField(column_name='uncertainty_confidenceEllipsoid_majorAxisAzimuth', null=True)
    uncertainty_confidenceellipsoid_majoraxisplunge = FloatField(column_name='uncertainty_confidenceEllipsoid_majorAxisPlunge', null=True)
    uncertainty_confidenceellipsoid_majoraxisrotation = FloatField(column_name='uncertainty_confidenceEllipsoid_majorAxisRotation', null=True)
    uncertainty_confidenceellipsoid_semiintermediateaxislength = FloatField(column_name='uncertainty_confidenceEllipsoid_semiIntermediateAxisLength', null=True)
    uncertainty_confidenceellipsoid_semimajoraxislength = FloatField(column_name='uncertainty_confidenceEllipsoid_semiMajorAxisLength', null=True)
    uncertainty_confidenceellipsoid_semiminoraxislength = FloatField(column_name='uncertainty_confidenceEllipsoid_semiMinorAxisLength', null=True)
    uncertainty_confidenceellipsoid_used = IntegerField(column_name='uncertainty_confidenceEllipsoid_used', constraints=[SQL("DEFAULT 0")])
    uncertainty_horizontaluncertainty = FloatField(column_name='uncertainty_horizontalUncertainty', null=True)
    uncertainty_maxhorizontaluncertainty = FloatField(column_name='uncertainty_maxHorizontalUncertainty', null=True)
    uncertainty_minhorizontaluncertainty = FloatField(column_name='uncertainty_minHorizontalUncertainty', null=True)
    uncertainty_preferreddescription = CharField(column_name='uncertainty_preferredDescription', null=True)
    uncertainty_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'Origin'

class Originreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    originid = CharField(column_name='originID', index=True)

    class Meta:
        table_name = 'OriginReference'
        indexes = (
            (('_parent_oid', 'originid'), True),
        )

class Outage(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    created = DateTimeField()
    created_ms = IntegerField()
    creatorid = CharField(column_name='creatorID')
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode')
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode')

    class Meta:
        table_name = 'Outage'
        indexes = (
            (('_parent_oid', 'waveformid_networkcode', 'waveformid_stationcode', 'waveformid_locationcode', 'waveformid_channelcode', 'waveformid_resourceuri', 'start', 'start_ms'), True),
        )

class Parameter(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    name = CharField()
    value = TextField(null=True)

    class Meta:
        table_name = 'Parameter'

class Parameterset(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    baseid = CharField(column_name='baseID', index=True, null=True)
    created = DateTimeField(null=True)
    created_ms = IntegerField(null=True)
    moduleid = CharField(column_name='moduleID', null=True)

    class Meta:
        table_name = 'ParameterSet'

class Pick(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    backazimuth_confidencelevel = FloatField(column_name='backazimuth_confidenceLevel', null=True)
    backazimuth_loweruncertainty = FloatField(column_name='backazimuth_lowerUncertainty', null=True)
    backazimuth_pdf_probability_content = TextField(null=True)
    backazimuth_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    backazimuth_pdf_variable_content = TextField(null=True)
    backazimuth_uncertainty = FloatField(null=True)
    backazimuth_upperuncertainty = FloatField(column_name='backazimuth_upperUncertainty', null=True)
    backazimuth_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    backazimuth_value = FloatField(null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    evaluationmode = CharField(column_name='evaluationMode', null=True)
    evaluationstatus = CharField(column_name='evaluationStatus', null=True)
    filterid = CharField(column_name='filterID', null=True)
    horizontalslowness_confidencelevel = FloatField(column_name='horizontalSlowness_confidenceLevel', null=True)
    horizontalslowness_loweruncertainty = FloatField(column_name='horizontalSlowness_lowerUncertainty', null=True)
    horizontalslowness_pdf_probability_content = TextField(column_name='horizontalSlowness_pdf_probability_content', null=True)
    horizontalslowness_pdf_used = IntegerField(column_name='horizontalSlowness_pdf_used', constraints=[SQL("DEFAULT 0")])
    horizontalslowness_pdf_variable_content = TextField(column_name='horizontalSlowness_pdf_variable_content', null=True)
    horizontalslowness_uncertainty = FloatField(column_name='horizontalSlowness_uncertainty', null=True)
    horizontalslowness_upperuncertainty = FloatField(column_name='horizontalSlowness_upperUncertainty', null=True)
    horizontalslowness_used = IntegerField(column_name='horizontalSlowness_used', constraints=[SQL("DEFAULT 0")])
    horizontalslowness_value = FloatField(column_name='horizontalSlowness_value', null=True)
    methodid = CharField(column_name='methodID', null=True)
    onset = CharField(null=True)
    phasehint_code = CharField(column_name='phaseHint_code', null=True)
    phasehint_used = IntegerField(column_name='phaseHint_used', constraints=[SQL("DEFAULT 0")])
    polarity = CharField(null=True)
    slownessmethodid = CharField(column_name='slownessMethodID', null=True)
    time_confidencelevel = FloatField(column_name='time_confidenceLevel', null=True)
    time_loweruncertainty = FloatField(column_name='time_lowerUncertainty', null=True)
    time_pdf_probability_content = TextField(null=True)
    time_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    time_pdf_variable_content = TextField(null=True)
    time_uncertainty = FloatField(null=True)
    time_upperuncertainty = FloatField(column_name='time_upperUncertainty', null=True)
    time_value = DateTimeField(index=True)
    time_value_ms = IntegerField(index=True)
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode')
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode')

    class Meta:
        table_name = 'Pick'

class Pickreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    pickid = CharField(column_name='pickID', index=True)

    class Meta:
        table_name = 'PickReference'
        indexes = (
            (('_parent_oid', 'pickid'), True),
        )

class Publicobject(BaseModel):
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    publicid = CharField(column_name='publicID', unique=True)

    class Meta:
        table_name = 'PublicObject'

class Qclog(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    created = DateTimeField()
    created_ms = IntegerField()
    creatorid = CharField(column_name='creatorID')
    end = DateTimeField()
    end_ms = IntegerField()
    message = TextField()
    start = DateTimeField()
    start_ms = IntegerField()
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode')
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode')

    class Meta:
        table_name = 'QCLog'
        indexes = (
            (('_parent_oid', 'start', 'start_ms', 'waveformid_networkcode', 'waveformid_stationcode', 'waveformid_locationcode', 'waveformid_channelcode', 'waveformid_resourceuri'), True),
        )

class Reading(BaseModel):
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)

    class Meta:
        table_name = 'Reading'

class Responsefap(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    name = CharField(null=True)
    numberoftuples = IntegerField(column_name='numberOfTuples', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    tuples_content = TextField(null=True)
    tuples_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'ResponseFAP'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Responsefir(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    coefficients_content = TextField(null=True)
    coefficients_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    correction = FloatField(null=True)
    decimationfactor = IntegerField(column_name='decimationFactor', null=True)
    delay = FloatField(null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    name = CharField(null=True)
    numberofcoefficients = IntegerField(column_name='numberOfCoefficients', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    symmetry = CharField(null=True)

    class Meta:
        table_name = 'ResponseFIR'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Responseiir(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    correction = FloatField(null=True)
    decimationfactor = IntegerField(column_name='decimationFactor', null=True)
    delay = FloatField(null=True)
    denominators_content = TextField(null=True)
    denominators_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    name = CharField(null=True)
    numberofdenominators = IntegerField(column_name='numberOfDenominators', null=True)
    numberofnumerators = IntegerField(column_name='numberOfNumerators', null=True)
    numerators_content = TextField(null=True)
    numerators_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    type = CharField(null=True)

    class Meta:
        table_name = 'ResponseIIR'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Responsepaz(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    correction = FloatField(null=True)
    decimationfactor = IntegerField(column_name='decimationFactor', null=True)
    delay = FloatField(null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    name = CharField(null=True)
    normalizationfactor = FloatField(column_name='normalizationFactor', null=True)
    normalizationfrequency = FloatField(column_name='normalizationFrequency', null=True)
    numberofpoles = IntegerField(column_name='numberOfPoles', null=True)
    numberofzeros = IntegerField(column_name='numberOfZeros', null=True)
    poles_content = TextField(null=True)
    poles_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    type = CharField(null=True)
    zeros_content = TextField(null=True)
    zeros_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'ResponsePAZ'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Responsepolynomial(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    approximationerror = FloatField(column_name='approximationError', null=True)
    approximationlowerbound = FloatField(column_name='approximationLowerBound', null=True)
    approximationtype = CharField(column_name='approximationType', null=True)
    approximationupperbound = FloatField(column_name='approximationUpperBound', null=True)
    coefficients_content = TextField(null=True)
    coefficients_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    frequencyunit = CharField(column_name='frequencyUnit', null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    name = CharField(null=True)
    numberofcoefficients = IntegerField(column_name='numberOfCoefficients', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'ResponsePolynomial'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Route(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    locationcode = CharField(column_name='locationCode')
    networkcode = CharField(column_name='networkCode')
    stationcode = CharField(column_name='stationCode')
    streamcode = CharField(column_name='streamCode')

    class Meta:
        table_name = 'Route'
        indexes = (
            (('_parent_oid', 'networkcode', 'stationcode', 'locationcode', 'streamcode'), True),
        )

class Routearclink(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    address = CharField()
    end = DateTimeField(null=True)
    priority = IntegerField(null=True)
    start = DateTimeField()

    class Meta:
        table_name = 'RouteArclink'
        indexes = (
            (('_parent_oid', 'address', 'start'), True),
        )

class Routeseedlink(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    address = CharField()
    priority = IntegerField(null=True)

    class Meta:
        table_name = 'RouteSeedlink'
        indexes = (
            (('_parent_oid', 'address'), True),
        )

class Sensor(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    description = CharField(null=True)
    highfrequency = FloatField(column_name='highFrequency', null=True)
    lowfrequency = FloatField(column_name='lowFrequency', null=True)
    manufacturer = CharField(null=True)
    model = CharField(null=True)
    name = CharField()
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    response = CharField(null=True)
    type = CharField(null=True)
    unit = CharField(null=True)

    class Meta:
        table_name = 'Sensor'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Sensorcalibration(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    channel = IntegerField()
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    serialnumber = CharField(column_name='serialNumber')
    start = DateTimeField()
    start_ms = IntegerField()

    class Meta:
        table_name = 'SensorCalibration'
        indexes = (
            (('_parent_oid', 'serialnumber', 'channel', 'start', 'start_ms'), True),
        )

class Sensorlocation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    code = CharField()
    elevation = FloatField(null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    latitude = FloatField(null=True)
    longitude = FloatField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()

    class Meta:
        table_name = 'SensorLocation'
        indexes = (
            (('_parent_oid', 'code', 'start', 'start_ms'), True),
        )

class Setup(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    enabled = IntegerField()
    name = CharField(null=True)
    parametersetid = CharField(column_name='parameterSetID', index=True, null=True)

    class Meta:
        table_name = 'Setup'
        indexes = (
            (('_parent_oid', 'name'), True),
        )

class Station(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    affiliation = CharField(null=True)
    archive = CharField(null=True)
    archivenetworkcode = CharField(column_name='archiveNetworkCode', null=True)
    code = CharField()
    country = CharField(null=True)
    description = CharField(null=True)
    elevation = FloatField(null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    latitude = FloatField(null=True)
    longitude = FloatField(null=True)
    place = CharField(null=True)
    remark_content = TextField(null=True)
    remark_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    restricted = IntegerField(null=True)
    shared = IntegerField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()
    type = CharField(null=True)

    class Meta:
        table_name = 'Station'
        indexes = (
            (('_parent_oid', 'code', 'start', 'start_ms'), True),
        )

class Stationgroup(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    code = CharField(null=True)
    description = CharField(null=True)
    elevation = FloatField(null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    latitude = FloatField(null=True)
    longitude = FloatField(null=True)
    start = DateTimeField(null=True)
    start_ms = IntegerField(null=True)
    type = CharField(null=True)

    class Meta:
        table_name = 'StationGroup'
        indexes = (
            (('_parent_oid', 'code'), True),
        )

class Stationmagnitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    amplitudeid = CharField(column_name='amplitudeID', index=True, null=True)
    creationinfo_agencyid = CharField(column_name='creationInfo_agencyID', null=True)
    creationinfo_agencyuri = CharField(column_name='creationInfo_agencyURI', null=True)
    creationinfo_author = CharField(column_name='creationInfo_author', null=True)
    creationinfo_authoruri = CharField(column_name='creationInfo_authorURI', null=True)
    creationinfo_creationtime = DateTimeField(column_name='creationInfo_creationTime', null=True)
    creationinfo_creationtime_ms = IntegerField(column_name='creationInfo_creationTime_ms', null=True)
    creationinfo_modificationtime = DateTimeField(column_name='creationInfo_modificationTime', null=True)
    creationinfo_modificationtime_ms = IntegerField(column_name='creationInfo_modificationTime_ms', null=True)
    creationinfo_used = IntegerField(column_name='creationInfo_used', constraints=[SQL("DEFAULT 0")])
    creationinfo_version = CharField(column_name='creationInfo_version', null=True)
    magnitude_confidencelevel = FloatField(column_name='magnitude_confidenceLevel', null=True)
    magnitude_loweruncertainty = FloatField(column_name='magnitude_lowerUncertainty', null=True)
    magnitude_pdf_probability_content = TextField(null=True)
    magnitude_pdf_used = IntegerField(constraints=[SQL("DEFAULT 0")])
    magnitude_pdf_variable_content = TextField(null=True)
    magnitude_uncertainty = FloatField(null=True)
    magnitude_upperuncertainty = FloatField(column_name='magnitude_upperUncertainty', null=True)
    magnitude_value = FloatField()
    methodid = CharField(column_name='methodID', null=True)
    originid = CharField(column_name='originID', null=True)
    type = CharField(null=True)
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode', null=True)
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode', null=True)
    waveformid_used = IntegerField(column_name='waveformID_used', constraints=[SQL("DEFAULT 0")])

    class Meta:
        table_name = 'StationMagnitude'

class Stationmagnitudecontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    residual = FloatField(null=True)
    stationmagnitudeid = CharField(column_name='stationMagnitudeID', index=True)
    weight = FloatField(null=True)

    class Meta:
        table_name = 'StationMagnitudeContribution'
        indexes = (
            (('_parent_oid', 'stationmagnitudeid'), True),
        )

class Stationreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    stationid = CharField(column_name='stationID', index=True)

    class Meta:
        table_name = 'StationReference'
        indexes = (
            (('_parent_oid', 'stationid'), True),
        )

class Stream(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    azimuth = FloatField(null=True)
    clockserialnumber = CharField(column_name='clockSerialNumber', null=True)
    code = CharField()
    datalogger = CharField(null=True)
    dataloggerchannel = IntegerField(column_name='dataloggerChannel', null=True)
    dataloggerserialnumber = CharField(column_name='dataloggerSerialNumber', null=True)
    depth = FloatField(null=True)
    dip = FloatField(null=True)
    end = DateTimeField(null=True)
    end_ms = IntegerField(null=True)
    flags = CharField(null=True)
    format = CharField(null=True)
    gain = FloatField(null=True)
    gainfrequency = FloatField(column_name='gainFrequency', null=True)
    gainunit = CharField(column_name='gainUnit', null=True)
    restricted = IntegerField(null=True)
    sampleratedenominator = IntegerField(column_name='sampleRateDenominator', null=True)
    sampleratenumerator = IntegerField(column_name='sampleRateNumerator', null=True)
    sensor = CharField(null=True)
    sensorchannel = IntegerField(column_name='sensorChannel', null=True)
    sensorserialnumber = CharField(column_name='sensorSerialNumber', null=True)
    shared = IntegerField(null=True)
    start = DateTimeField()
    start_ms = IntegerField()

    class Meta:
        table_name = 'Stream'
        indexes = (
            (('_parent_oid', 'code', 'start', 'start_ms'), True),
        )

class Waveformquality(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT CURRENT_TIMESTAMP")])
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = IntegerField(index=True)
    created = DateTimeField()
    created_ms = IntegerField()
    creatorid = CharField(column_name='creatorID')
    end = DateTimeField(index=True, null=True)
    end_ms = IntegerField(index=True, null=True)
    loweruncertainty = FloatField(column_name='lowerUncertainty', null=True)
    parameter = CharField()
    start = DateTimeField(index=True)
    start_ms = IntegerField(index=True)
    type = CharField()
    upperuncertainty = FloatField(column_name='upperUncertainty', null=True)
    value = FloatField()
    waveformid_channelcode = CharField(column_name='waveformID_channelCode', null=True)
    waveformid_locationcode = CharField(column_name='waveformID_locationCode', null=True)
    waveformid_networkcode = CharField(column_name='waveformID_networkCode')
    waveformid_resourceuri = CharField(column_name='waveformID_resourceURI', null=True)
    waveformid_stationcode = CharField(column_name='waveformID_stationCode')
    windowlength = FloatField(column_name='windowLength', null=True)

    class Meta:
        table_name = 'WaveformQuality'
        indexes = (
            (('_parent_oid', 'start', 'start_ms', 'waveformid_networkcode', 'waveformid_stationcode', 'waveformid_locationcode', 'waveformid_channelcode', 'waveformid_resourceuri', 'type', 'parameter'), True),
        )

