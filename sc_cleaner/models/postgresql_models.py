"""
Models have been generated with this command line:

sudo -u postgres python3 -m pwiz --engine postgresql database
"""

from peewee import *
from .base_models import BaseModel

class UnknownField(object):
    def __init__(self, *_, **__): pass

class Object(BaseModel):
    _oid = BigAutoField()
    _timestamp = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)

    class Meta:
        table_name = 'object'

class Access(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_end = DateTimeField(null=True)
    m_locationcode = CharField()
    m_networkcode = CharField()
    m_start = DateTimeField()
    m_stationcode = CharField()
    m_streamcode = CharField()
    m_user = CharField()

    class Meta:
        table_name = 'access'
        indexes = (
            (('_parent_oid', 'm_networkcode', 'm_stationcode', 'm_locationcode', 'm_streamcode', 'm_user', 'm_start'), True),
        )

class Amplitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_amplitude_confidencelevel = FloatField(null=True)
    m_amplitude_loweruncertainty = FloatField(null=True)
    m_amplitude_pdf_probability_content = BlobField(null=True)
    m_amplitude_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_amplitude_pdf_variable_content = BlobField(null=True)
    m_amplitude_uncertainty = FloatField(null=True)
    m_amplitude_upperuncertainty = FloatField(null=True)
    m_amplitude_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_amplitude_value = FloatField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_evaluationmode = CharField(null=True)
    m_filterid = CharField(null=True)
    m_magnitudehint = CharField(null=True)
    m_methodid = CharField(null=True)
    m_period_confidencelevel = FloatField(null=True)
    m_period_loweruncertainty = FloatField(null=True)
    m_period_pdf_probability_content = BlobField(null=True)
    m_period_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_period_pdf_variable_content = BlobField(null=True)
    m_period_uncertainty = FloatField(null=True)
    m_period_upperuncertainty = FloatField(null=True)
    m_period_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_period_value = FloatField(null=True)
    m_pickid = CharField(index=True, null=True)
    m_scalingtime_confidencelevel = FloatField(null=True)
    m_scalingtime_loweruncertainty = FloatField(null=True)
    m_scalingtime_pdf_probability_content = BlobField(null=True)
    m_scalingtime_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_scalingtime_pdf_variable_content = BlobField(null=True)
    m_scalingtime_uncertainty = FloatField(null=True)
    m_scalingtime_upperuncertainty = FloatField(null=True)
    m_scalingtime_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_scalingtime_value = DateTimeField(null=True)
    m_scalingtime_value_ms = IntegerField(null=True)
    m_snr = FloatField(null=True)
    m_timewindow_begin = FloatField(null=True)
    m_timewindow_end = FloatField(null=True)
    m_timewindow_reference = DateTimeField(index=True, null=True)
    m_timewindow_reference_ms = IntegerField(index=True, null=True)
    m_timewindow_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_type = CharField()
    m_unit = CharField(null=True)
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField(null=True)
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField(null=True)
    m_waveformid_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'amplitude'

class Amplitudereference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_amplitudeid = CharField(index=True)

    class Meta:
        table_name = 'amplitudereference'
        indexes = (
            (('_parent_oid', 'm_amplitudeid'), True),
        )

class Arclinkrequest(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_clientid = CharField(null=True)
    m_clientip = CharField(null=True)
    m_created = DateTimeField()
    m_created_ms = IntegerField()
    m_header = CharField(null=True)
    m_label = CharField(null=True)
    m_message = CharField(null=True)
    m_requestid = CharField()
    m_status = CharField()
    m_summary_averagetimewindow = IntegerField(null=True)
    m_summary_oklinecount = IntegerField(null=True)
    m_summary_totallinecount = IntegerField(null=True)
    m_summary_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_type = CharField()
    m_userid = CharField()
    m_userip = CharField(null=True)

    class Meta:
        table_name = 'arclinkrequest'
        indexes = (
            (('_parent_oid', 'm_requestid', 'm_userid', 'm_created', 'm_created_ms'), True),
        )

class Arclinkrequestline(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_constraints = CharField(null=True)
    m_end = DateTimeField()
    m_end_ms = IntegerField()
    m_netclass = CharField(null=True)
    m_restricted = BooleanField(null=True)
    m_shared = BooleanField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_status_message = CharField(null=True)
    m_status_size = IntegerField(null=True)
    m_status_status = CharField()
    m_status_type = CharField()
    m_status_volumeid = CharField(null=True)
    m_streamid_channelcode = CharField(null=True)
    m_streamid_locationcode = CharField(null=True)
    m_streamid_networkcode = CharField()
    m_streamid_resourceuri = CharField(null=True)
    m_streamid_stationcode = CharField()

    class Meta:
        table_name = 'arclinkrequestline'
        indexes = (
            (('_parent_oid', 'm_start', 'm_start_ms', 'm_end', 'm_end_ms', 'm_streamid_networkcode', 'm_streamid_stationcode', 'm_streamid_locationcode', 'm_streamid_channelcode', 'm_streamid_resourceuri'), True),
        )

class Arclinkstatusline(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_message = CharField(null=True)
    m_size = IntegerField(null=True)
    m_status = CharField()
    m_type = CharField()
    m_volumeid = CharField(null=True)

    class Meta:
        table_name = 'arclinkstatusline'
        indexes = (
            (('_parent_oid', 'm_type', 'm_status', 'm_volumeid'), True),
        )

class Arclinkuser(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_email = CharField(null=True)
    m_name = CharField()
    m_password = CharField(null=True)

    class Meta:
        table_name = 'arclinkuser'
        indexes = (
            (('_parent_oid', 'm_name', 'm_email'), True),
        )

class Arrival(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_azimuth = FloatField(null=True)
    m_backazimuthresidual = FloatField(null=True)
    m_backazimuthused = BooleanField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_distance = FloatField(null=True)
    m_earthmodelid = CharField(null=True)
    m_horizontalslownessresidual = FloatField(null=True)
    m_horizontalslownessused = BooleanField(null=True)
    m_phase_code = CharField()
    m_pickid = CharField(index=True)
    m_preliminary = BooleanField(null=True)
    m_takeoffangle = FloatField(null=True)
    m_timecorrection = FloatField(null=True)
    m_timeresidual = FloatField(null=True)
    m_timeused = BooleanField(null=True)
    m_weight = FloatField(null=True)

    class Meta:
        table_name = 'arrival'
        indexes = (
            (('_parent_oid', 'm_pickid'), True),
        )

class Auxdevice(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_description = CharField(null=True)
    m_manufacturer = CharField(null=True)
    m_model = CharField(null=True)
    m_name = CharField()
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'auxdevice'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Auxsource(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_conversion = CharField(null=True)
    m_description = CharField(null=True)
    m_name = CharField()
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_sampleratedenominator = IntegerField(null=True)
    m_sampleratenumerator = IntegerField(null=True)
    m_unit = CharField(null=True)

    class Meta:
        table_name = 'auxsource'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Auxstream(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_code = CharField()
    m_device = CharField(null=True)
    m_deviceserialnumber = CharField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_flags = CharField(null=True)
    m_format = CharField(null=True)
    m_restricted = BooleanField(null=True)
    m_shared = BooleanField(null=True)
    m_source = CharField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()

    class Meta:
        table_name = 'auxstream'
        indexes = (
            (('_parent_oid', 'm_code', 'm_start', 'm_start_ms'), True),
        )

class Comment(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_id = CharField(null=True)
    m_start = DateTimeField(null=True)
    m_start_ms = IntegerField(null=True)
    m_text = BlobField()

    class Meta:
        table_name = 'comment'
        indexes = (
            (('_parent_oid', 'm_id'), True),
        )

class Compositetime(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_day_confidencelevel = FloatField(null=True)
    m_day_loweruncertainty = IntegerField(null=True)
    m_day_uncertainty = IntegerField(null=True)
    m_day_upperuncertainty = IntegerField(null=True)
    m_day_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_day_value = IntegerField(null=True)
    m_hour_confidencelevel = FloatField(null=True)
    m_hour_loweruncertainty = IntegerField(null=True)
    m_hour_uncertainty = IntegerField(null=True)
    m_hour_upperuncertainty = IntegerField(null=True)
    m_hour_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_hour_value = IntegerField(null=True)
    m_minute_confidencelevel = FloatField(null=True)
    m_minute_loweruncertainty = IntegerField(null=True)
    m_minute_uncertainty = IntegerField(null=True)
    m_minute_upperuncertainty = IntegerField(null=True)
    m_minute_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_minute_value = IntegerField(null=True)
    m_month_confidencelevel = FloatField(null=True)
    m_month_loweruncertainty = IntegerField(null=True)
    m_month_uncertainty = IntegerField(null=True)
    m_month_upperuncertainty = IntegerField(null=True)
    m_month_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_month_value = IntegerField(null=True)
    m_second_confidencelevel = FloatField(null=True)
    m_second_loweruncertainty = FloatField(null=True)
    m_second_pdf_probability_content = BlobField(null=True)
    m_second_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_second_pdf_variable_content = BlobField(null=True)
    m_second_uncertainty = FloatField(null=True)
    m_second_upperuncertainty = FloatField(null=True)
    m_second_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_second_value = FloatField(null=True)
    m_year_confidencelevel = FloatField(null=True)
    m_year_loweruncertainty = IntegerField(null=True)
    m_year_uncertainty = IntegerField(null=True)
    m_year_upperuncertainty = IntegerField(null=True)
    m_year_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_year_value = IntegerField(null=True)

    class Meta:
        table_name = 'compositetime'

class Configmodule(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_enabled = BooleanField()
    m_name = CharField()
    m_parametersetid = CharField(index=True, null=True)

    class Meta:
        table_name = 'configmodule'

class Configstation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_enabled = BooleanField()
    m_networkcode = CharField()
    m_stationcode = CharField()

    class Meta:
        table_name = 'configstation'
        indexes = (
            (('_parent_oid', 'm_networkcode', 'm_stationcode'), True),
        )

class Dataattributeextent(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_end = DateTimeField()
    m_end_ms = IntegerField()
    m_quality = CharField()
    m_samplerate = FloatField()
    m_segmentcount = IntegerField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_updated = DateTimeField()
    m_updated_ms = IntegerField()

    class Meta:
        table_name = 'dataattributeextent'
        indexes = (
            (('m_end', 'm_end_ms'), False),
            (('m_quality', 'm_samplerate', '_parent_oid'), True),
            (('m_start', 'm_start_ms'), False),
            (('m_updated', 'm_updated_ms'), False),
        )

class Dataextent(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_end = DateTimeField()
    m_end_ms = IntegerField()
    m_lastscan = DateTimeField()
    m_lastscan_ms = IntegerField()
    m_segmentoverflow = BooleanField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_updated = DateTimeField()
    m_updated_ms = IntegerField()
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField()
    m_waveformid_resourceuri = CharField(index=True, null=True)
    m_waveformid_stationcode = CharField()

    class Meta:
        table_name = 'dataextent'
        indexes = (
            (('m_end_ms', 'm_end'), False),
            (('m_lastscan_ms', 'm_lastscan'), False),
            (('m_start', 'm_start_ms'), False),
            (('m_updated', 'm_updated_ms'), False),
            (('m_waveformid_resourceuri', '_parent_oid', 'm_waveformid_networkcode', 'm_waveformid_stationcode', 'm_waveformid_locationcode', 'm_waveformid_channelcode'), True),
        )

class Datalogger(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_clockmanufacturer = CharField(null=True)
    m_clockmodel = CharField(null=True)
    m_clocktype = CharField(null=True)
    m_description = CharField(null=True)
    m_digitizermanufacturer = CharField(null=True)
    m_digitizermodel = CharField(null=True)
    m_gain = FloatField(null=True)
    m_maxclockdrift = FloatField(null=True)
    m_name = CharField(null=True)
    m_recordermanufacturer = CharField(null=True)
    m_recordermodel = CharField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'datalogger'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Dataloggercalibration(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_channel = IntegerField()
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_serialnumber = CharField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()

    class Meta:
        table_name = 'dataloggercalibration'
        indexes = (
            (('_parent_oid', 'm_serialnumber', 'm_channel', 'm_start', 'm_start_ms'), True),
        )

class Datasegment(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_end = DateTimeField()
    m_end_ms = IntegerField()
    m_outoforder = BooleanField()
    m_quality = CharField()
    m_samplerate = FloatField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_updated = DateTimeField()
    m_updated_ms = IntegerField()

    class Meta:
        table_name = 'datasegment'
        indexes = (
            (('m_end', 'm_end_ms'), False),
            (('m_start', 'm_start_ms'), False),
            (('m_start_ms', 'm_start', '_parent_oid'), True),
            (('m_updated', 'm_updated_ms'), False),
        )

class Dataused(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_componentcount = IntegerField()
    m_shortestperiod = FloatField(null=True)
    m_stationcount = IntegerField()
    m_wavetype = CharField()

    class Meta:
        table_name = 'dataused'

class Decimation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_analoguefilterchain_content = BlobField(null=True)
    m_analoguefilterchain_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_digitalfilterchain_content = BlobField(null=True)
    m_digitalfilterchain_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_sampleratedenominator = IntegerField()
    m_sampleratenumerator = IntegerField()

    class Meta:
        table_name = 'decimation'
        indexes = (
            (('_parent_oid', 'm_sampleratenumerator', 'm_sampleratedenominator'), True),
        )

class Event(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_preferredfocalmechanismid = CharField(index=True, null=True)
    m_preferredmagnitudeid = CharField(index=True, null=True)
    m_preferredoriginid = CharField(index=True, null=True)
    m_type = CharField(null=True)
    m_typecertainty = CharField(null=True)

    class Meta:
        table_name = 'event'

class Eventdescription(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_text = CharField()
    m_type = CharField()

    class Meta:
        table_name = 'eventdescription'
        indexes = (
            (('_parent_oid', 'm_type'), True),
        )

class Focalmechanism(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_azimuthalgap = FloatField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_evaluationmode = CharField(null=True)
    m_evaluationstatus = CharField(null=True)
    m_methodid = CharField(null=True)
    m_misfit = FloatField(null=True)
    m_nodalplanes_nodalplane1_dip_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane1_dip_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_dip_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_dip_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane1_dip_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_dip_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_dip_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_dip_value = FloatField(null=True)
    m_nodalplanes_nodalplane1_rake_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane1_rake_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_rake_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_rake_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane1_rake_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_rake_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_rake_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_rake_value = FloatField(null=True)
    m_nodalplanes_nodalplane1_strike_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane1_strike_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_strike_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_strike_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane1_strike_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane1_strike_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_strike_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane1_strike_value = FloatField(null=True)
    m_nodalplanes_nodalplane1_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane2_dip_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane2_dip_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_dip_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_dip_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane2_dip_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_dip_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_dip_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_dip_value = FloatField(null=True)
    m_nodalplanes_nodalplane2_rake_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane2_rake_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_rake_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_rake_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane2_rake_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_rake_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_rake_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_rake_value = FloatField(null=True)
    m_nodalplanes_nodalplane2_strike_confidencelevel = FloatField(null=True)
    m_nodalplanes_nodalplane2_strike_loweruncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_strike_pdf_probability_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_strike_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_nodalplane2_strike_pdf_variable_content = BlobField(null=True)
    m_nodalplanes_nodalplane2_strike_uncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_strike_upperuncertainty = FloatField(null=True)
    m_nodalplanes_nodalplane2_strike_value = FloatField(null=True)
    m_nodalplanes_nodalplane2_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_nodalplanes_preferredplane = IntegerField(null=True)
    m_nodalplanes_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_naxis_azimuth_confidencelevel = FloatField(null=True)
    m_principalaxes_naxis_azimuth_loweruncertainty = FloatField(null=True)
    m_principalaxes_naxis_azimuth_pdf_probability_content = BlobField(null=True)
    m_principalaxes_naxis_azimuth_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_naxis_azimuth_pdf_variable_content = BlobField(null=True)
    m_principalaxes_naxis_azimuth_uncertainty = FloatField(null=True)
    m_principalaxes_naxis_azimuth_upperuncertainty = FloatField(null=True)
    m_principalaxes_naxis_azimuth_value = FloatField(null=True)
    m_principalaxes_naxis_length_confidencelevel = FloatField(null=True)
    m_principalaxes_naxis_length_loweruncertainty = FloatField(null=True)
    m_principalaxes_naxis_length_pdf_probability_content = BlobField(null=True)
    m_principalaxes_naxis_length_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_naxis_length_pdf_variable_content = BlobField(null=True)
    m_principalaxes_naxis_length_uncertainty = FloatField(null=True)
    m_principalaxes_naxis_length_upperuncertainty = FloatField(null=True)
    m_principalaxes_naxis_length_value = FloatField(null=True)
    m_principalaxes_naxis_plunge_confidencelevel = FloatField(null=True)
    m_principalaxes_naxis_plunge_loweruncertainty = FloatField(null=True)
    m_principalaxes_naxis_plunge_pdf_probability_content = BlobField(null=True)
    m_principalaxes_naxis_plunge_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_naxis_plunge_pdf_variable_content = BlobField(null=True)
    m_principalaxes_naxis_plunge_uncertainty = FloatField(null=True)
    m_principalaxes_naxis_plunge_upperuncertainty = FloatField(null=True)
    m_principalaxes_naxis_plunge_value = FloatField(null=True)
    m_principalaxes_naxis_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_paxis_azimuth_confidencelevel = FloatField(null=True)
    m_principalaxes_paxis_azimuth_loweruncertainty = FloatField(null=True)
    m_principalaxes_paxis_azimuth_pdf_probability_content = BlobField(null=True)
    m_principalaxes_paxis_azimuth_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_paxis_azimuth_pdf_variable_content = BlobField(null=True)
    m_principalaxes_paxis_azimuth_uncertainty = FloatField(null=True)
    m_principalaxes_paxis_azimuth_upperuncertainty = FloatField(null=True)
    m_principalaxes_paxis_azimuth_value = FloatField(null=True)
    m_principalaxes_paxis_length_confidencelevel = FloatField(null=True)
    m_principalaxes_paxis_length_loweruncertainty = FloatField(null=True)
    m_principalaxes_paxis_length_pdf_probability_content = BlobField(null=True)
    m_principalaxes_paxis_length_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_paxis_length_pdf_variable_content = BlobField(null=True)
    m_principalaxes_paxis_length_uncertainty = FloatField(null=True)
    m_principalaxes_paxis_length_upperuncertainty = FloatField(null=True)
    m_principalaxes_paxis_length_value = FloatField(null=True)
    m_principalaxes_paxis_plunge_confidencelevel = FloatField(null=True)
    m_principalaxes_paxis_plunge_loweruncertainty = FloatField(null=True)
    m_principalaxes_paxis_plunge_pdf_probability_content = BlobField(null=True)
    m_principalaxes_paxis_plunge_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_paxis_plunge_pdf_variable_content = BlobField(null=True)
    m_principalaxes_paxis_plunge_uncertainty = FloatField(null=True)
    m_principalaxes_paxis_plunge_upperuncertainty = FloatField(null=True)
    m_principalaxes_paxis_plunge_value = FloatField(null=True)
    m_principalaxes_taxis_azimuth_confidencelevel = FloatField(null=True)
    m_principalaxes_taxis_azimuth_loweruncertainty = FloatField(null=True)
    m_principalaxes_taxis_azimuth_pdf_probability_content = BlobField(null=True)
    m_principalaxes_taxis_azimuth_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_taxis_azimuth_pdf_variable_content = BlobField(null=True)
    m_principalaxes_taxis_azimuth_uncertainty = FloatField(null=True)
    m_principalaxes_taxis_azimuth_upperuncertainty = FloatField(null=True)
    m_principalaxes_taxis_azimuth_value = FloatField(null=True)
    m_principalaxes_taxis_length_confidencelevel = FloatField(null=True)
    m_principalaxes_taxis_length_loweruncertainty = FloatField(null=True)
    m_principalaxes_taxis_length_pdf_probability_content = BlobField(null=True)
    m_principalaxes_taxis_length_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_taxis_length_pdf_variable_content = BlobField(null=True)
    m_principalaxes_taxis_length_uncertainty = FloatField(null=True)
    m_principalaxes_taxis_length_upperuncertainty = FloatField(null=True)
    m_principalaxes_taxis_length_value = FloatField(null=True)
    m_principalaxes_taxis_plunge_confidencelevel = FloatField(null=True)
    m_principalaxes_taxis_plunge_loweruncertainty = FloatField(null=True)
    m_principalaxes_taxis_plunge_pdf_probability_content = BlobField(null=True)
    m_principalaxes_taxis_plunge_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_principalaxes_taxis_plunge_pdf_variable_content = BlobField(null=True)
    m_principalaxes_taxis_plunge_uncertainty = FloatField(null=True)
    m_principalaxes_taxis_plunge_upperuncertainty = FloatField(null=True)
    m_principalaxes_taxis_plunge_value = FloatField(null=True)
    m_principalaxes_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_stationdistributionratio = FloatField(null=True)
    m_stationpolaritycount = IntegerField(null=True)
    m_triggeringoriginid = CharField(index=True, null=True)

    class Meta:
        table_name = 'focalmechanism'

class Focalmechanismreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_focalmechanismid = CharField(index=True)

    class Meta:
        table_name = 'focalmechanismreference'
        indexes = (
            (('_parent_oid', 'm_focalmechanismid'), True),
        )

class Journalentry(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_action = CharField()
    m_created = DateTimeField(null=True)
    m_created_ms = IntegerField(null=True)
    m_objectid = CharField(index=True)
    m_parameters = CharField(null=True)
    m_sender = CharField()

    class Meta:
        table_name = 'journalentry'

class Magnitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_azimuthalgap = FloatField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_evaluationstatus = CharField(null=True)
    m_magnitude_confidencelevel = FloatField(null=True)
    m_magnitude_loweruncertainty = FloatField(null=True)
    m_magnitude_pdf_probability_content = BlobField(null=True)
    m_magnitude_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_magnitude_pdf_variable_content = BlobField(null=True)
    m_magnitude_uncertainty = FloatField(null=True)
    m_magnitude_upperuncertainty = FloatField(null=True)
    m_magnitude_value = FloatField()
    m_methodid = CharField(null=True)
    m_originid = CharField(null=True)
    m_stationcount = IntegerField(null=True)
    m_type = CharField(null=True)

    class Meta:
        table_name = 'magnitude'

class Meta(BaseModel):
    name = CharField(primary_key=True)
    value = CharField()

    class Meta:
        table_name = 'meta'

class Momenttensor(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_clvd = FloatField(null=True)
    m_cmtname = CharField(null=True)
    m_cmtversion = CharField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_derivedoriginid = CharField(index=True)
    m_doublecouple = FloatField(null=True)
    m_filterid = CharField(null=True)
    m_greensfunctionid = CharField(null=True)
    m_iso = FloatField(null=True)
    m_method = CharField(null=True)
    m_methodid = CharField(null=True)
    m_momentmagnitudeid = CharField(null=True)
    m_scalarmoment_confidencelevel = FloatField(null=True)
    m_scalarmoment_loweruncertainty = FloatField(null=True)
    m_scalarmoment_pdf_probability_content = BlobField(null=True)
    m_scalarmoment_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_scalarmoment_pdf_variable_content = BlobField(null=True)
    m_scalarmoment_uncertainty = FloatField(null=True)
    m_scalarmoment_upperuncertainty = FloatField(null=True)
    m_scalarmoment_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_scalarmoment_value = FloatField(null=True)
    m_sourcetimefunction_decaytime = FloatField(null=True)
    m_sourcetimefunction_duration = FloatField(null=True)
    m_sourcetimefunction_risetime = FloatField(null=True)
    m_sourcetimefunction_type = CharField(null=True)
    m_sourcetimefunction_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_status = CharField(null=True)
    m_tensor_mpp_confidencelevel = FloatField(null=True)
    m_tensor_mpp_loweruncertainty = FloatField(null=True)
    m_tensor_mpp_pdf_probability_content = BlobField(null=True)
    m_tensor_mpp_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mpp_pdf_variable_content = BlobField(null=True)
    m_tensor_mpp_uncertainty = FloatField(null=True)
    m_tensor_mpp_upperuncertainty = FloatField(null=True)
    m_tensor_mpp_value = FloatField(null=True)
    m_tensor_mrp_confidencelevel = FloatField(null=True)
    m_tensor_mrp_loweruncertainty = FloatField(null=True)
    m_tensor_mrp_pdf_probability_content = BlobField(null=True)
    m_tensor_mrp_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mrp_pdf_variable_content = BlobField(null=True)
    m_tensor_mrp_uncertainty = FloatField(null=True)
    m_tensor_mrp_upperuncertainty = FloatField(null=True)
    m_tensor_mrp_value = FloatField(null=True)
    m_tensor_mrr_confidencelevel = FloatField(null=True)
    m_tensor_mrr_loweruncertainty = FloatField(null=True)
    m_tensor_mrr_pdf_probability_content = BlobField(null=True)
    m_tensor_mrr_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mrr_pdf_variable_content = BlobField(null=True)
    m_tensor_mrr_uncertainty = FloatField(null=True)
    m_tensor_mrr_upperuncertainty = FloatField(null=True)
    m_tensor_mrr_value = FloatField(null=True)
    m_tensor_mrt_confidencelevel = FloatField(null=True)
    m_tensor_mrt_loweruncertainty = FloatField(null=True)
    m_tensor_mrt_pdf_probability_content = BlobField(null=True)
    m_tensor_mrt_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mrt_pdf_variable_content = BlobField(null=True)
    m_tensor_mrt_uncertainty = FloatField(null=True)
    m_tensor_mrt_upperuncertainty = FloatField(null=True)
    m_tensor_mrt_value = FloatField(null=True)
    m_tensor_mtp_confidencelevel = FloatField(null=True)
    m_tensor_mtp_loweruncertainty = FloatField(null=True)
    m_tensor_mtp_pdf_probability_content = BlobField(null=True)
    m_tensor_mtp_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mtp_pdf_variable_content = BlobField(null=True)
    m_tensor_mtp_uncertainty = FloatField(null=True)
    m_tensor_mtp_upperuncertainty = FloatField(null=True)
    m_tensor_mtp_value = FloatField(null=True)
    m_tensor_mtt_confidencelevel = FloatField(null=True)
    m_tensor_mtt_loweruncertainty = FloatField(null=True)
    m_tensor_mtt_pdf_probability_content = BlobField(null=True)
    m_tensor_mtt_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tensor_mtt_pdf_variable_content = BlobField(null=True)
    m_tensor_mtt_uncertainty = FloatField(null=True)
    m_tensor_mtt_upperuncertainty = FloatField(null=True)
    m_tensor_mtt_value = FloatField(null=True)
    m_tensor_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_variance = FloatField(null=True)
    m_variancereduction = FloatField(null=True)

    class Meta:
        table_name = 'momenttensor'

class Momenttensorcomponentcontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_active = BooleanField()
    m_component = IntegerField()
    m_datatimewindow = BlobField()
    m_misfit = FloatField(null=True)
    m_phasecode = CharField()
    m_snr = FloatField(null=True)
    m_timeshift = FloatField()
    m_weight = FloatField()

    class Meta:
        table_name = 'momenttensorcomponentcontribution'
        indexes = (
            (('_parent_oid', 'm_phasecode', 'm_component'), True),
        )

class Momenttensorphasesetting(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_code = CharField()
    m_lowerperiod = FloatField()
    m_maximumtimeshift = FloatField(null=True)
    m_minimumsnr = FloatField(null=True)
    m_upperperiod = FloatField()

    class Meta:
        table_name = 'momenttensorphasesetting'
        indexes = (
            (('_parent_oid', 'm_code'), True),
        )

class Momenttensorstationcontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_active = BooleanField()
    m_timeshift = FloatField(null=True)
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField(null=True)
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField(null=True)
    m_waveformid_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_weight = FloatField(null=True)

    class Meta:
        table_name = 'momenttensorstationcontribution'

class Network(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_archive = CharField(null=True)
    m_code = CharField()
    m_description = CharField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_institutions = CharField(null=True)
    m_netclass = CharField(null=True)
    m_region = CharField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_restricted = BooleanField(null=True)
    m_shared = BooleanField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_type = CharField(null=True)

    class Meta:
        table_name = 'network'
        indexes = (
            (('_parent_oid', 'm_code', 'm_start', 'm_start_ms'), True),
        )

class Origin(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_depth_confidencelevel = FloatField(null=True)
    m_depth_loweruncertainty = FloatField(null=True)
    m_depth_pdf_probability_content = BlobField(null=True)
    m_depth_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_depth_pdf_variable_content = BlobField(null=True)
    m_depth_uncertainty = FloatField(null=True)
    m_depth_upperuncertainty = FloatField(null=True)
    m_depth_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_depth_value = FloatField(null=True)
    m_depthtype = CharField(null=True)
    m_earthmodelid = CharField(null=True)
    m_epicenterfixed = BooleanField(null=True)
    m_evaluationmode = CharField(null=True)
    m_evaluationstatus = CharField(null=True)
    m_latitude_confidencelevel = FloatField(null=True)
    m_latitude_loweruncertainty = FloatField(null=True)
    m_latitude_pdf_probability_content = BlobField(null=True)
    m_latitude_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_latitude_pdf_variable_content = BlobField(null=True)
    m_latitude_uncertainty = FloatField(null=True)
    m_latitude_upperuncertainty = FloatField(null=True)
    m_latitude_value = FloatField()
    m_longitude_confidencelevel = FloatField(null=True)
    m_longitude_loweruncertainty = FloatField(null=True)
    m_longitude_pdf_probability_content = BlobField(null=True)
    m_longitude_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_longitude_pdf_variable_content = BlobField(null=True)
    m_longitude_uncertainty = FloatField(null=True)
    m_longitude_upperuncertainty = FloatField(null=True)
    m_longitude_value = FloatField()
    m_methodid = CharField(null=True)
    m_quality_associatedphasecount = IntegerField(null=True)
    m_quality_associatedstationcount = IntegerField(null=True)
    m_quality_azimuthalgap = FloatField(null=True)
    m_quality_depthphasecount = IntegerField(null=True)
    m_quality_groundtruthlevel = CharField(null=True)
    m_quality_maximumdistance = FloatField(null=True)
    m_quality_mediandistance = FloatField(null=True)
    m_quality_minimumdistance = FloatField(null=True)
    m_quality_secondaryazimuthalgap = FloatField(null=True)
    m_quality_standarderror = FloatField(null=True)
    m_quality_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_quality_usedphasecount = IntegerField(null=True)
    m_quality_usedstationcount = IntegerField(null=True)
    m_referencesystemid = CharField(null=True)
    m_time_confidencelevel = FloatField(null=True)
    m_time_loweruncertainty = FloatField(null=True)
    m_time_pdf_probability_content = BlobField(null=True)
    m_time_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_time_pdf_variable_content = BlobField(null=True)
    m_time_uncertainty = FloatField(null=True)
    m_time_upperuncertainty = FloatField(null=True)
    m_time_value = DateTimeField(index=True)
    m_time_value_ms = IntegerField(index=True)
    m_timefixed = BooleanField(null=True)
    m_type = CharField(null=True)
    m_uncertainty_azimuthmaxhorizontaluncertainty = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_majoraxisazimuth = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_majoraxisplunge = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_majoraxisrotation = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_semiintermediateaxislength = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_semimajoraxislength = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_semiminoraxislength = FloatField(null=True)
    m_uncertainty_confidenceellipsoid_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_uncertainty_horizontaluncertainty = FloatField(null=True)
    m_uncertainty_maxhorizontaluncertainty = FloatField(null=True)
    m_uncertainty_minhorizontaluncertainty = FloatField(null=True)
    m_uncertainty_preferreddescription = CharField(null=True)
    m_uncertainty_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'origin'

class Originreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_originid = CharField(index=True)

    class Meta:
        table_name = 'originreference'
        indexes = (
            (('_parent_oid', 'm_originid'), True),
        )

class Outage(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_created = DateTimeField()
    m_created_ms = IntegerField()
    m_creatorid = CharField()
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField()
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField()

    class Meta:
        table_name = 'outage'
        indexes = (
            (('_parent_oid', 'm_waveformid_networkcode', 'm_waveformid_stationcode', 'm_waveformid_locationcode', 'm_waveformid_channelcode', 'm_waveformid_resourceuri', 'm_start', 'm_start_ms'), True),
        )

class Parameter(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_name = CharField()
    m_value = BlobField(null=True)

    class Meta:
        table_name = 'parameter'

class Parameterset(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_baseid = CharField(index=True, null=True)
    m_created = DateTimeField(null=True)
    m_created_ms = IntegerField(null=True)
    m_moduleid = CharField(null=True)

    class Meta:
        table_name = 'parameterset'

class Pick(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_backazimuth_confidencelevel = FloatField(null=True)
    m_backazimuth_loweruncertainty = FloatField(null=True)
    m_backazimuth_pdf_probability_content = BlobField(null=True)
    m_backazimuth_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_backazimuth_pdf_variable_content = BlobField(null=True)
    m_backazimuth_uncertainty = FloatField(null=True)
    m_backazimuth_upperuncertainty = FloatField(null=True)
    m_backazimuth_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_backazimuth_value = FloatField(null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_evaluationmode = CharField(null=True)
    m_evaluationstatus = CharField(null=True)
    m_filterid = CharField(null=True)
    m_horizontalslowness_confidencelevel = FloatField(null=True)
    m_horizontalslowness_loweruncertainty = FloatField(null=True)
    m_horizontalslowness_pdf_probability_content = BlobField(null=True)
    m_horizontalslowness_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_horizontalslowness_pdf_variable_content = BlobField(null=True)
    m_horizontalslowness_uncertainty = FloatField(null=True)
    m_horizontalslowness_upperuncertainty = FloatField(null=True)
    m_horizontalslowness_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_horizontalslowness_value = FloatField(null=True)
    m_methodid = CharField(null=True)
    m_onset = CharField(null=True)
    m_phasehint_code = CharField(null=True)
    m_phasehint_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_polarity = CharField(null=True)
    m_slownessmethodid = CharField(null=True)
    m_time_confidencelevel = FloatField(null=True)
    m_time_loweruncertainty = FloatField(null=True)
    m_time_pdf_probability_content = BlobField(null=True)
    m_time_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_time_pdf_variable_content = BlobField(null=True)
    m_time_uncertainty = FloatField(null=True)
    m_time_upperuncertainty = FloatField(null=True)
    m_time_value = DateTimeField(index=True)
    m_time_value_ms = IntegerField(index=True)
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField()
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField()

    class Meta:
        table_name = 'pick'

class Pickreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_pickid = CharField(index=True)

    class Meta:
        table_name = 'pickreference'
        indexes = (
            (('_parent_oid', 'm_pickid'), True),
        )

class Publicobject(BaseModel):
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    m_publicid = CharField(unique=True)

    class Meta:
        table_name = 'publicobject'

class Qclog(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_created = DateTimeField()
    m_created_ms = IntegerField()
    m_creatorid = CharField()
    m_end = DateTimeField()
    m_end_ms = IntegerField()
    m_message = BlobField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField()
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField()

    class Meta:
        table_name = 'qclog'
        indexes = (
            (('_parent_oid', 'm_waveformid_networkcode', 'm_waveformid_stationcode', 'm_waveformid_locationcode', 'm_waveformid_channelcode', 'm_waveformid_resourceuri', 'm_start', 'm_start_ms'), True),
        )

class Reading(BaseModel):
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)

    class Meta:
        table_name = 'reading'

class Responsefap(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_name = CharField(null=True)
    m_numberoftuples = IntegerField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_tuples_content = BlobField(null=True)
    m_tuples_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'responsefap'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Responsefir(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_coefficients_content = BlobField(null=True)
    m_coefficients_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_correction = FloatField(null=True)
    m_decimationfactor = IntegerField(null=True)
    m_delay = FloatField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_name = CharField(null=True)
    m_numberofcoefficients = IntegerField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_symmetry = CharField(null=True)

    class Meta:
        table_name = 'responsefir'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Responseiir(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_correction = FloatField(null=True)
    m_decimationfactor = IntegerField(null=True)
    m_delay = FloatField(null=True)
    m_denominators_content = BlobField(null=True)
    m_denominators_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_name = CharField(null=True)
    m_numberofdenominators = IntegerField(null=True)
    m_numberofnumerators = IntegerField(null=True)
    m_numerators_content = BlobField(null=True)
    m_numerators_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_type = CharField(null=True)

    class Meta:
        table_name = 'responseiir'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Responsepaz(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_correction = FloatField(null=True)
    m_decimationfactor = IntegerField(null=True)
    m_delay = FloatField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_name = CharField(null=True)
    m_normalizationfactor = FloatField(null=True)
    m_normalizationfrequency = FloatField(null=True)
    m_numberofpoles = IntegerField(null=True)
    m_numberofzeros = IntegerField(null=True)
    m_poles_content = BlobField(null=True)
    m_poles_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_type = CharField(null=True)
    m_zeros_content = BlobField(null=True)
    m_zeros_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'responsepaz'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Responsepolynomial(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_approximationerror = FloatField(null=True)
    m_approximationlowerbound = FloatField(null=True)
    m_approximationtype = CharField(null=True)
    m_approximationupperbound = FloatField(null=True)
    m_coefficients_content = BlobField(null=True)
    m_coefficients_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_frequencyunit = CharField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_name = CharField(null=True)
    m_numberofcoefficients = IntegerField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'responsepolynomial'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Route(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_locationcode = CharField()
    m_networkcode = CharField()
    m_stationcode = CharField()
    m_streamcode = CharField()

    class Meta:
        table_name = 'route'
        indexes = (
            (('_parent_oid', 'm_networkcode', 'm_stationcode', 'm_locationcode', 'm_streamcode'), True),
        )

class Routearclink(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_address = CharField()
    m_end = DateTimeField(null=True)
    m_priority = IntegerField(null=True)
    m_start = DateTimeField()

    class Meta:
        table_name = 'routearclink'
        indexes = (
            (('_parent_oid', 'm_address', 'm_start'), True),
        )

class Routeseedlink(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_address = CharField()
    m_priority = IntegerField(null=True)

    class Meta:
        table_name = 'routeseedlink'
        indexes = (
            (('_parent_oid', 'm_address'), True),
        )

class Sensor(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_description = CharField(null=True)
    m_highfrequency = FloatField(null=True)
    m_lowfrequency = FloatField(null=True)
    m_manufacturer = CharField(null=True)
    m_model = CharField(null=True)
    m_name = CharField()
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_response = CharField(null=True)
    m_type = CharField(null=True)
    m_unit = CharField(null=True)

    class Meta:
        table_name = 'sensor'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Sensorcalibration(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_channel = IntegerField()
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_serialnumber = CharField()
    m_start = DateTimeField()
    m_start_ms = IntegerField()

    class Meta:
        table_name = 'sensorcalibration'
        indexes = (
            (('_parent_oid', 'm_serialnumber', 'm_channel', 'm_start', 'm_start_ms'), True),
        )

class Sensorlocation(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_code = CharField()
    m_elevation = FloatField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_latitude = FloatField(null=True)
    m_longitude = FloatField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()

    class Meta:
        table_name = 'sensorlocation'
        indexes = (
            (('_parent_oid', 'm_code', 'm_start', 'm_start_ms'), True),
        )

class Setup(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_enabled = BooleanField()
    m_name = CharField(null=True)
    m_parametersetid = CharField(index=True, null=True)

    class Meta:
        table_name = 'setup'
        indexes = (
            (('_parent_oid', 'm_name'), True),
        )

class Station(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_affiliation = CharField(null=True)
    m_archive = CharField(null=True)
    m_archivenetworkcode = CharField(null=True)
    m_code = CharField()
    m_country = CharField(null=True)
    m_description = CharField(null=True)
    m_elevation = FloatField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_latitude = FloatField(null=True)
    m_longitude = FloatField(null=True)
    m_place = CharField(null=True)
    m_remark_content = BlobField(null=True)
    m_remark_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_restricted = BooleanField(null=True)
    m_shared = BooleanField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()
    m_type = CharField(null=True)

    class Meta:
        table_name = 'station'
        indexes = (
            (('_parent_oid', 'm_code', 'm_start', 'm_start_ms'), True),
        )

class Stationgroup(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_code = CharField(null=True)
    m_description = CharField(null=True)
    m_elevation = FloatField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_latitude = FloatField(null=True)
    m_longitude = FloatField(null=True)
    m_start = DateTimeField(null=True)
    m_start_ms = IntegerField(null=True)
    m_type = CharField(null=True)

    class Meta:
        table_name = 'stationgroup'
        indexes = (
            (('_parent_oid', 'm_code'), True),
        )

class Stationmagnitude(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_amplitudeid = CharField(index=True, null=True)
    m_creationinfo_agencyid = CharField(null=True)
    m_creationinfo_agencyuri = CharField(null=True)
    m_creationinfo_author = CharField(null=True)
    m_creationinfo_authoruri = CharField(null=True)
    m_creationinfo_creationtime = DateTimeField(null=True)
    m_creationinfo_creationtime_ms = IntegerField(null=True)
    m_creationinfo_modificationtime = DateTimeField(null=True)
    m_creationinfo_modificationtime_ms = IntegerField(null=True)
    m_creationinfo_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_creationinfo_version = CharField(null=True)
    m_magnitude_confidencelevel = FloatField(null=True)
    m_magnitude_loweruncertainty = FloatField(null=True)
    m_magnitude_pdf_probability_content = BlobField(null=True)
    m_magnitude_pdf_used = BooleanField(constraints=[SQL("DEFAULT false")])
    m_magnitude_pdf_variable_content = BlobField(null=True)
    m_magnitude_uncertainty = FloatField(null=True)
    m_magnitude_upperuncertainty = FloatField(null=True)
    m_magnitude_value = FloatField()
    m_methodid = CharField(null=True)
    m_originid = CharField(null=True)
    m_passedqc = BooleanField(null=True)
    m_type = CharField(null=True)
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField(null=True)
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField(null=True)
    m_waveformid_used = BooleanField(constraints=[SQL("DEFAULT false")])

    class Meta:
        table_name = 'stationmagnitude'

class Stationmagnitudecontribution(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_residual = FloatField(null=True)
    m_stationmagnitudeid = CharField(index=True)
    m_weight = FloatField(null=True)

    class Meta:
        table_name = 'stationmagnitudecontribution'
        indexes = (
            (('_parent_oid', 'm_stationmagnitudeid'), True),
        )

class Stationreference(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_stationid = CharField(index=True)

    class Meta:
        table_name = 'stationreference'
        indexes = (
            (('_parent_oid', 'm_stationid'), True),
        )

class Stream(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_azimuth = FloatField(null=True)
    m_clockserialnumber = CharField(null=True)
    m_code = CharField()
    m_datalogger = CharField(null=True)
    m_dataloggerchannel = IntegerField(null=True)
    m_dataloggerserialnumber = CharField(null=True)
    m_depth = FloatField(null=True)
    m_dip = FloatField(null=True)
    m_end = DateTimeField(null=True)
    m_end_ms = IntegerField(null=True)
    m_flags = CharField(null=True)
    m_format = CharField(null=True)
    m_gain = FloatField(null=True)
    m_gainfrequency = FloatField(null=True)
    m_gainunit = CharField(null=True)
    m_restricted = BooleanField(null=True)
    m_sampleratedenominator = IntegerField(null=True)
    m_sampleratenumerator = IntegerField(null=True)
    m_sensor = CharField(null=True)
    m_sensorchannel = IntegerField(null=True)
    m_sensorserialnumber = CharField(null=True)
    m_shared = BooleanField(null=True)
    m_start = DateTimeField()
    m_start_ms = IntegerField()

    class Meta:
        table_name = 'stream'
        indexes = (
            (('_parent_oid', 'm_code', 'm_start', 'm_start_ms'), True),
        )

class Waveformquality(BaseModel):
    _last_modified = DateTimeField(constraints=[SQL("DEFAULT now()")], null=True)
    _oid = ForeignKeyField(column_name='_oid', field='_oid', model=Object, primary_key=True)
    _parent_oid = BigIntegerField(index=True)
    m_created = DateTimeField()
    m_created_ms = IntegerField()
    m_creatorid = CharField()
    m_end = DateTimeField(index=True, null=True)
    m_end_ms = IntegerField(index=True, null=True)
    m_loweruncertainty = FloatField(null=True)
    m_parameter = CharField()
    m_start = DateTimeField(index=True)
    m_start_ms = IntegerField(index=True)
    m_type = CharField()
    m_upperuncertainty = FloatField(null=True)
    m_value = FloatField()
    m_waveformid_channelcode = CharField(null=True)
    m_waveformid_locationcode = CharField(null=True)
    m_waveformid_networkcode = CharField()
    m_waveformid_resourceuri = CharField(null=True)
    m_waveformid_stationcode = CharField()
    m_windowlength = FloatField(null=True)

    class Meta:
        table_name = 'waveformquality'
        indexes = (
            (('_parent_oid', 'm_waveformid_networkcode', 'm_waveformid_stationcode', 'm_waveformid_locationcode', 'm_waveformid_channelcode', 'm_waveformid_resourceuri', 'm_start', 'm_start_ms', 'm_type', 'm_parameter'), True),
        )

