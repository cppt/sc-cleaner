# Seiscomp database model creation using peewee

Use your read/write user on your database seiscomp, (refer to your seiscomp configuration and documentation).

```bash
python3 -m pwiz --info -o -u USER -P PASSWD --engine postgresql seiscomp > postgresql_model.0.11.py
```
